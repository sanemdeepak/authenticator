package com.authenticator.repo

import com.authenticator.exception.AuthenticatorException
import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.modal.User
import com.authenticator.repository.UserRepository
import com.authenticator.repository.impl.UserRepositoryImpl
import com.authenticator.utilities.rowMappers.UserRowMapper
import org.springframework.dao.DataAccessException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import spock.lang.Shared
import spock.lang.Specification

import java.awt.datatransfer.MimeTypeParseException
import java.sql.SQLDataException
import java.sql.SQLException

/**
 * Created by sanemdeepak on 4/20/17.
 */
class UserRepositorySpec extends Specification {

    @Shared
    private JdbcTemplate jdbcTemplate;

    private UserRowMapper userRowMapper;

    private UserRepository userRepository

    def setup() {
        jdbcTemplate = Mock(JdbcTemplate)
        userRowMapper = Mock(UserRowMapper)
        userRepository = new UserRepositoryImpl(jdbcTemplate, userRowMapper)
    }

    def "Happy path - createUser"() {

        setup:
        User user = getValidUser()

        when:
        def maybeUser = userRepository.createUser(user)

        then:
        1 * jdbcTemplate.update(*_) >> 1
        1 * jdbcTemplate.queryForObject(*_) >> user

        maybeUser.get().userLinkId == user.userLinkId
    }

    def "Destructive path [Test - 1 {When a invalid user is passed}] - createUser"() {
        setup:
        User user = getInvalidUser()

        when:
        def maybeUser = userRepository.createUser(user)

        then:
        !maybeUser.isPresent()
        0 * jdbcTemplate.update(*_)
        0 * jdbcTemplate.queryForObject(*_)
    }

    def "Destructive path [Test - 2 {When jdbc update fails}] - createUser"() {
        setup:
        User user = getValidUser()

        when:
        userRepository.createUser(user)

        then:
        1 * jdbcTemplate.update(*_) >> 0
        thrown(AuthenticatorResourceException)
    }

    def "Destructive path [Test - 3 {When jdbc update passes but user is not found in database}] - createUser"() {
        setup:
        User user = getValidUser()

        when:
        userRepository.createUser(user)

        then:
        1 * jdbcTemplate.update(*_) >> 1
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - findUserByUserLinkId"() {
        setup:
        User user = getValidUser();

        when:
        def maybeUser = userRepository.findUserByUserLinkId("validUserLinkId")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> user
        maybeUser.get().getUserLinkId() == user.getUserLinkId()
    }

    def "Destructive path - findUserByUserLinkId {empty userlinkid}"() {

        when:
        def maybeUser = userRepository.findUserByUserLinkId("")

        then:
        0 * jdbcTemplate.queryForObject(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findUserByUserLinkId {No user with userlinkid}"() {

        when:
        def maybeUser = userRepository.findUserByUserLinkId("noUserUserLinkId")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - findUserByUsername"() {
        setup:
        User user = getValidUser();

        when:
        def maybeUser = userRepository.findUserByUsername("validUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> user
        maybeUser.get().getUserLinkId() == user.getUserLinkId()
    }

    def "Destructive path - findUserByUsername {empty username}"() {

        when:
        def maybeUser = userRepository.findUserByUsername("")

        then:
        0 * jdbcTemplate.queryForObject(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findUserByUsername {No user with username}"() {

        when:
        def maybeUser = userRepository.findUserByUsername("noUserUserLinkId")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - findPasswordByUsername"() {
        setup:
        User user = getValidUser();

        when:
        def maybeUser = userRepository.findPasswordByUsername("validUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> user
        maybeUser.get().getUserLinkId() == user.getUserLinkId()
    }

    def "Destructive path - findPasswordByUsername {empty username}"() {

        when:
        def maybeUser = userRepository.findPasswordByUsername("")

        then:
        0 * jdbcTemplate.queryForObject(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findPasswordByUsername {No user with username}"() {

        when:
        def maybeUser = userRepository.findPasswordByUsername("noUserUserUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - findUserLinkIdByUsername"() {
        setup:
        User user = getValidUser();

        when:
        def maybeUser = userRepository.findUserLinkIdByUsername("findUserLinkIdByUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> user
        maybeUser.get().getUserLinkId() == user.getUserLinkId()
    }

    def "Destructive path - findUserLinkIdByUsername {empty userlinkid}"() {

        when:
        def maybeUser = userRepository.findUserLinkIdByUsername("")

        then:
        0 * jdbcTemplate.queryForObject(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findUserLinkIdByUsername {No user with username}"() {

        when:
        def maybeUser = userRepository.findUserLinkIdByUsername("noUserUserUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - findOrgIdByUsername"() {
        setup:
        User user = getValidUser();

        when:
        def maybeUser = userRepository.findOrgIdByUsername("findOrgIdByUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> user
        maybeUser.get().getUserLinkId() == user.getUserLinkId()
    }

    def "Destructive path - findOrgIdByUsername {empty userlinkid}"() {

        when:
        def maybeUser = userRepository.findOrgIdByUsername("")

        then:
        0 * jdbcTemplate.queryForObject(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findOrgIdByUsername {No user with username}"() {

        when:
        def maybeUser = userRepository.findOrgIdByUsername("noUserUserUsername")

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - updateUserDetails()"() {
        setup:
        User validUser = getValidUser()

        when:
        def mayBeUpdatedUser = userRepository.updateUserDetails(validUser, "userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        1 * jdbcTemplate.queryForObject(*_) >> validUser
        mayBeUpdatedUser.get().getUserLinkId() == validUser.getUserLinkId()
    }

    def "Destructive path - updateUserDetails() {invalid user}"() {
        setup:
        User validUser = getInvalidUser()

        when:
        def mayBeUpdatedUser = userRepository.updateUserDetails(validUser, "userLinkId")

        then:
        0 * jdbcTemplate.update(*_) >> 1
        !mayBeUpdatedUser.isPresent()
    }

    def "Destructive path - updateUserDetails() {update fails}"() {
        setup:
        User validUser = getInvalidUser()

        when:
        def mayBeUpdatedUser = userRepository.updateUserDetails(validUser, "userLinkId")

        then:
        0 * jdbcTemplate.update(*_) >> 0
        !mayBeUpdatedUser.isPresent()
    }

    def "Destructive path - updateUserDetails() {update throws exception}"() {
        setup:
        User validUser = getValidUser()

        when:
        userRepository.updateUserDetails(validUser, "userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }

    def "Destructive path - updateUserDetails() {update succeeded but findUserByUserLinkId fails }"() {
        setup:
        User validUser = getValidUser()

        when:
        userRepository.updateUserDetails(validUser, "userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path -  updateUserPassword"() {
        when:
        boolean result = userRepository.updateUserPassword("userLinkId", "password")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive Path -  updateUserPassword {empty details}"() {
        when:
        boolean result = userRepository.updateUserPassword("", "")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive Path -  updateUserPassword {update failed}"() {
        when:
        boolean result = userRepository.updateUserPassword("userLinkId", "password")

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Destructive Path -  updateUserPassword {jdbc throws exception}"() {
        when:
        userRepository.updateUserPassword("userLinkId", "password")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }


    def "Happy path -  updateUserUsername"() {
        when:
        boolean result = userRepository.updateUserUsername("userLinkId", "username")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive Path -  updateUserUsername {empty details}"() {
        when:
        boolean result = userRepository.updateUserUsername("", "")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive Path -  updateUserUsername {update failed}"() {
        when:
        boolean result = userRepository.updateUserUsername("userLinkId", "username")

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Destructive Path -  updateUserUsername {jdbc throws exception}"() {
        when:
        userRepository.updateUserUsername("userLinkId", "username")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }


    def "Happy path -  activateUser"() {
        when:
        boolean result = userRepository.activateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive Path -  activateUser {empty details}"() {
        when:
        boolean result = userRepository.activateUser("")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive Path -  activateUser {update failed}"() {
        when:
        boolean result = userRepository.activateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Destructive Path -  activateUser {jdbc throws exception}"() {
        when:
        userRepository.activateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }


    def "Happy path -  deactivateUser"() {
        when:
        boolean result = userRepository.deactivateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive Path -  deactivateUser {empty details}"() {
        when:
        boolean result = userRepository.deactivateUser("")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive Path -  deactivateUser {update failed}"() {
        when:
        boolean result = userRepository.deactivateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Destructive Path -  deactivateUser {jdbc throws exception}"() {
        when:
        userRepository.deactivateUser("userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }


    def "Happy path - findAllUsersByOrganizationId"() {
        setup:
        List<User> userList = new ArrayList<>()
        userList.add(getValidUser())

        when:
        def maybeUser = userRepository.findAllUsersByOrganizationId("validOrgId")

        then:
        1 * jdbcTemplate.query(*_) >> userList
        maybeUser.get().get(0).getUserLinkId() == userList.get(0).getUserLinkId()

    }

    def "Destructive path - findAllUsersByOrganizationId {empty orgId}"() {

        when:
        def maybeUser = userRepository.findAllUsersByOrganizationId("")

        then:
        0 * jdbcTemplate.query(*_)
        !maybeUser.isPresent()
    }

    def "Destructive path - findAllUsersByOrganizationId {query throws exception}"() {
        setup:
        DataAccessException dataAccessException = Mock(DataAccessException)
        when:
        def maybeUser = userRepository.findAllUsersByOrganizationId("validOrgId")

        then:
        1 * jdbcTemplate.query(*_) >> { throw dataAccessException }
        thrown(AuthenticatorResourceException)

    }


    private User getValidUser() {

        String randomString = UUID.randomUUID().toString()
        User user = new User()
        user.userLinkId = randomString
        user.username = randomString
        user.password = randomString
        user.firstName = randomString
        user.lastName = randomString
        user.email = randomString
        user.phoneNumber = randomString
        user.address = randomString
        user.orgId = randomString

        return user
    }

    private User getInvalidUser() {

        String randomString = UUID.randomUUID().toString()
        User user = new User()
        user.userLinkId = randomString
        user.username = randomString
        user.password = randomString
        user.email = randomString
        user.phoneNumber = randomString
        user.address = randomString
        user.orgId = randomString

        return user
    }
}
