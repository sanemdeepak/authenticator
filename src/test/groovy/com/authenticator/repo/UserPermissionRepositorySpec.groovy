package com.authenticator.repo

import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.modal.UserPermission
import com.authenticator.repository.UserPermissionRepository
import com.authenticator.repository.impl.UserPermissionRepositoryImpl
import com.authenticator.utilities.rowMappers.UserPermissionRowMapper
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import spock.lang.Specification

/**
 * @author DEEPAK SANEM 
 * @date 5/1/17.
 */
class UserPermissionRepositorySpec extends Specification {
    private JdbcTemplate jdbcTemplate;
    private UserPermissionRepository userPermissionRepository;
    private UserPermissionRowMapper userPermissionRowMapper;


    def setup() {
        jdbcTemplate = Mock(JdbcTemplate)
        userPermissionRowMapper = Mock(UserPermissionRowMapper)
        userPermissionRepository = new UserPermissionRepositoryImpl(jdbcTemplate, userPermissionRowMapper)
    }

    def "Happy path - findAllUserPermissionsByUserLinkId"() {
        setup:
        UserPermission userPermission = new UserPermission()
        userPermission.setUserLinkId(UUID.randomUUID().toString())
        List<UserPermission> userPermissionList = new ArrayList<>()
        userPermissionList.add(userPermission)

        when:
        def result = userPermissionRepository.findAllUserPermissionsByUserLinkId("userLinkId")

        then:
        1 * jdbcTemplate.query(*_) >> userPermissionList
        result.get().get(0).getUserLinkId() == userPermission.getUserLinkId()
    }

    def "Destructive path - findAllUserPermissionsByUserLinkId {empty userLinkId}"() {
        when:
        def result = userPermissionRepository.findAllUserPermissionsByUserLinkId("")

        then:
        0 * jdbcTemplate.query(*_)
        !result.isPresent()
    }

    def "Destructive path - findAllUserPermissionsByUserLinkId {jdbc throws exception}"() {
        setup:
        DataAccessException dataAccessException = Mock(DataAccessException)

        when:
        def result = userPermissionRepository.findAllUserPermissionsByUserLinkId("userLinkId")

        then:
        1 * jdbcTemplate.query(*_) >> { throw dataAccessException }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - addNewUserPermission"() {
        setup:
        UserPermission userPermission = getValidUserPermission()

        when:
        def result = userPermissionRepository.addNewUserPermission(userPermission)

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path - addNewUserPermission {invalid UserPermission}"() {
        setup:
        UserPermission userPermission = getInvalidUserPermission()

        when:
        def result = userPermissionRepository.addNewUserPermission(userPermission)

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive path - addNewUserPermission {jdbc throws exception}"() {
        setup:
        UserPermission userPermission = getValidUserPermission()
        DataAccessException dataAccessException = Mock(DataAccessException)

        when:
        userPermissionRepository.addNewUserPermission(userPermission)

        then:
        1 * jdbcTemplate.update(*_) >> { throw dataAccessException }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - addAllUserPermissions"() {
        setup:
        List<UserPermission> userPermissionList = new ArrayList<>()
        userPermissionList.add(getValidUserPermission())
        userPermissionList.add(getValidUserPermission())

        when:
        def result = userPermissionRepository.addAllUserPermissions(userPermissionList)

        then:
        2 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path - addAllUserPermissions {invalid UserPermission}"() {
        setup:
        List<UserPermission> userPermissionList = new ArrayList<>()
        userPermissionList.add(getValidUserPermission())
        userPermissionList.add(getInvalidUserPermission())

        when:
        def result = userPermissionRepository.addAllUserPermissions(userPermissionList)

        then:
        1 * jdbcTemplate.update(*_) >> 1
        !result
    }

    def "Happy path - activateUserPermission"() {
        when:
        def result = userPermissionRepository.activateUserPermission("permission", "userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path - activateUserPermission {empty arguments}"() {
        when:
        def result = userPermissionRepository.deactivateUserPermission("", "")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }


    def "Happy path - deactivateUserPermission"() {
        when:
        def result = userPermissionRepository.deactivateUserPermission("permission", "userLinkId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path - deactivateUserPermission {empty arguments}"() {
        when:
        def result = userPermissionRepository.activateUserPermission("", "")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }


    private UserPermission getValidUserPermission() {

        UserPermission userPermission = new UserPermission()
        userPermission.setPermissionId(UUID.randomUUID().toString())
        userPermission.setPermission("validPermission")
        userPermission.setUserLinkId(UUID.randomUUID().toString())
        userPermission.setActive(true)

        return userPermission;
    }

    private UserPermission getInvalidUserPermission() {

        UserPermission userPermission = new UserPermission()
        userPermission.setPermission("validPermission")
        userPermission.setActive(true)

        return userPermission;
    }
}
