package com.authenticator.repo

import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.modal.Invitation
import com.authenticator.repository.InvitationRepository
import com.authenticator.repository.impl.InvitationRepositoryImpl
import com.authenticator.utilities.rowMappers.InvitationRowMapper
import org.springframework.dao.DataAccessException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import spock.lang.Shared
import spock.lang.Specification

import java.sql.Timestamp
import java.time.Instant

/**
 * Created by sanemdeepak on 3/30/17.
 */
class InvitationRepositoryImplSpec extends Specification {

    @Shared
    JdbcTemplate jdbcTemplate

    private InvitationRepository invitationRepository
    private InvitationRowMapper invitationRowMapper

    def setup() {
        this.jdbcTemplate = Mock(JdbcTemplate)
        this.invitationRowMapper = Mock(InvitationRowMapper)
        this.invitationRepository = new InvitationRepositoryImpl(jdbcTemplate, invitationRowMapper)
    }

    def "Happy path - findInvitationByInvitationId"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.getUserLinkId() >> "8144b48f-ad7f-488a-9516-fdb766a64e37"
        String invitationId = "dummyId"

        when:
        Optional<Invitation> result = invitationRepository.findInvitationByInvitationId(invitationId)

        then:
        1 * jdbcTemplate.queryForObject(*_) >> invitation
        result.isPresent()
        result.get().getUserLinkId() == "8144b48f-ad7f-488a-9516-fdb766a64e37"
    }

    def "Destructive path[Test 1] - findInvitationByInvitationId"() {
        setup:
        String invitationId = ""

        when:
        Optional<Invitation> result = invitationRepository.findInvitationByInvitationId(invitationId)

        then:
        !result.isPresent()
    }

    def "Destructive path[Test 2] - findInvitationByInvitationId"() {
        setup:
        String invitationId = "abcd"

        when:
        Optional<Invitation> result = invitationRepository.findInvitationByInvitationId(invitationId)

        then:
        1 * jdbcTemplate.queryForObject(*_) >> { throw new EmptyResultDataAccessException(1) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path createNewInvitation"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.getInvitationId() >> "id"
        invitation.getUserLinkId() >> "userlinkid"
        invitation.getCreatedByUserLinkId() >> "createdById"
        Timestamp timestamp = Mock(Timestamp)
        timestamp.before(*_) >> false
        invitation.getExpiryTimestamp() >> timestamp

        when:
        boolean result = invitationRepository.createNewInvitation(invitation)

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path[Test-1] -  createNewInvitation"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.getInvitationId() >> ""
        invitation.getUserLinkId() >> ""
        invitation.getCreatedByUserLinkId() >> ""
        Timestamp timestamp = Mock(Timestamp)
        timestamp.before(*_) >> true
        invitation.getExpiryTimestamp() >> timestamp

        when:
        boolean result = invitationRepository.createNewInvitation(invitation)

        then:
        0 * jdbcTemplate.update(*_) >> 1
        !result
    }

    def "Destructive path[Test-2] -  createNewInvitation"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.getInvitationId() >> "id"
        invitation.getUserLinkId() >> "userlinkid"
        invitation.getCreatedByUserLinkId() >> "createdById"
        Timestamp timestamp = Mock(Timestamp)
        timestamp.before(*_) >> false
        invitation.getExpiryTimestamp() >> timestamp

        when:
        boolean result = invitationRepository.createNewInvitation(invitation)

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Happy path - invalidateInvitationByInvitationId"() {
        when:
        boolean result = invitationRepository.invalidateInvitationByInvitationId("invitationid")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path[Test - 1] - invalidateInvitationByInvitationId"() {
        when:
        boolean result = invitationRepository.invalidateInvitationByInvitationId("")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive path[Test - 2] - invalidateInvitationByInvitationId"() {
        when:
        boolean result = invitationRepository.invalidateInvitationByInvitationId("invitationId")

        then:
        1 * jdbcTemplate.update(*_) >> 0
        !result
    }

    def "Happy path - claimInvitation()"() {
        when:
        boolean result = invitationRepository.claimInvitation("validInvitationId", Timestamp.from(Instant.now()))

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path[Test - 1] claimInvitation()"() {
        when:
        boolean result = invitationRepository.claimInvitation("", Timestamp.from(Instant.now()))

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive path[Test - 2] claimInvitation()"() {
        when:
        invitationRepository.claimInvitation("validInvitationId", Timestamp.from(Instant.now()))

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path - rollbackInvitationClaim()"() {
        when:
        boolean result = invitationRepository.rollbackInvitationClaim("validInvitationId")

        then:
        1 * jdbcTemplate.update(*_) >> 1
        result
    }

    def "Destructive path[Test - 1] rollbackInvitationClaim()"() {
        when:
        boolean result = invitationRepository.rollbackInvitationClaim("")

        then:
        0 * jdbcTemplate.update(*_)
        !result
    }

    def "Destructive path[Test - 2] rollbackInvitationClaim()"() {
        when:
        invitationRepository.rollbackInvitationClaim("validInvitationId")

        then:
        1 * jdbcTemplate.update(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorResourceException)
    }
}
