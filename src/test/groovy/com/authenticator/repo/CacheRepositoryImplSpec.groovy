package com.authenticator.repo

import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.repository.impl.CacheRepositoryImpl
import com.authenticator.repository.CacheRepository
import com.fasterxml.jackson.databind.ObjectMapper
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.exceptions.JedisConnectionException
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by sanemdeepak on 4/18/17.
 */
public class CacheRepositoryImplSpec extends Specification {

    @Shared
    Jedis jedis

    @Shared
    JedisPool jedisPool

    CacheRepository cacheRepository

    ObjectMapper objectMapper

    def setup() {
        this.jedisPool = Mock(JedisPool)
        this.objectMapper = Mock(ObjectMapper)
        this.jedis = Mock(Jedis)
        this.jedisPool.getResource() >> jedis
        this.cacheRepository = new CacheRepositoryImpl(jedisPool, objectMapper)
    }

    def "Happy path - setToken"() {
        when:
        boolean result = cacheRepository.setToken("jwt", "username")

        then:
        1 * jedis.hset(*_) >> 1
        result
    }

    def "Destructive path[Test - 1] - setToken"() {
        when:
        boolean result = cacheRepository.setToken("", "")

        then:
        0 * jedis.set(*_)
        !result
    }

    def "Destructive path[Test - 2] - setToken"() {
        when:
        boolean result = cacheRepository.setToken("token", "username")

        then:
        1 * jedis.hset(*_) >> -1
        !result
    }

    def "Destructive path[Test - 3] - setToken"() {
        when:
        boolean result = cacheRepository.setToken("token", "username")

        then:
        1 * jedis.hset(*_) >> { throw new JedisConnectionException("") }
        thrown(AuthenticatorResourceException)
    }

    def "Happy path -  getTokens"() {

        setup:
        Map<String, String> resultMap = new HashMap<>()
        resultMap.put("key", "value")

        when:
        def result = cacheRepository.getTokens("username")

        then:
        1 * jedis.hgetAll(*_) >> resultMap
        result.isPresent()
    }

    def "Destructive path [Test -1] -  getTokens"() {

        setup:
        Map<String, String> resultMap = null

        when:
        def result = cacheRepository.getTokens("username")

        then:
        1 * jedis.hgetAll(*_) >> resultMap
        !result.isPresent()
    }

    def "Destructive path [Test - 2]  -  getTokens"() {

        when:
        def result = cacheRepository.getTokens("username")

        then:
        1 * jedis.hgetAll(*_) >> { throw new JedisConnectionException("") }
        thrown(AuthenticatorResourceException)
    }

    def "Destructive path [Test - 3]  -  getTokens"() {

        when:
        def result = cacheRepository.getTokens("")

        then:
        !result.isPresent()
    }

    def "Happy path - unsetToken"() {
        when:
        boolean result = cacheRepository.unsetToken("tokenId", "username")

        then:
        1 * jedis.hdel(*_) >> 1
        result
    }

    def "Destructive path [Test - 1] unsetToken"(){
        when:
        boolean result = cacheRepository.unsetToken("", "")

        then:
        0 * jedis.hdel(*_)
        !result
    }

    def "Destructive path [Test - 2] unsetToken"(){
        when:
        boolean result = cacheRepository.unsetToken("tokenId", "username")

        then:
        1 * jedis.hdel(*_) >> 0
        !result
    }

    def "Destructive path [Test - 3] unsetToken"(){
        when:
        boolean result = cacheRepository.unsetToken("tokenId", "username")

        then:
        1 * jedis.hdel(*_) >> {throw new JedisConnectionException("")}
        thrown(AuthenticatorResourceException)
    }
}
