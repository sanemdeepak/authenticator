package com.authenticator.service

import com.authenticator.exception.AuthenticatorServiceException
import com.authenticator.modal.JWTToken
import com.authenticator.repository.CacheRepository
import com.authenticator.repository.impl.CacheRepositoryImpl
import com.authenticator.utilities.jwt.JWTUtil
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import spock.lang.Specification

/**
 * Created by sanemdeepak on 5/8/17.
 */
class TokenAuthServiceSpec extends Specification {

    CacheRepository cacheRepository;
    JWTUtil jwtUtil;

    TokenAuthService tokenAuthService;

    def setup() {
        cacheRepository = Mock(CacheRepositoryImpl)
        jwtUtil = Mock(JWTUtil)
        tokenAuthService = new TokenAuthService(cacheRepository, jwtUtil);
    }

    def "Happy path - isAuthenticated()"() {
        setup:
        Map map = new HashMap();
        map.put("username", "validToken");

        JWTToken jwtToken = Mock(JWTToken)
        jwtToken.getUsername() >> "username"

        when:
        def result = tokenAuthService.isAuthenticated("bearer validToken");

        then:
        1 * cacheRepository.getTokens(*_) >> Optional.of(map);
        2 * jwtUtil.parseJWT(*_) >> jwtToken
        result.isPresent();
    }

    def "Destructive path - token does not exist in cache"() {
        setup:
        JWTToken jwtToken = Mock(JWTToken)
        jwtToken.getUsername() >> "username"

        when:
        def result = tokenAuthService.isAuthenticated("bearer validToken");

        then:
        1 * cacheRepository.getTokens(*_) >> Optional.empty();
        1 * jwtUtil.parseJWT(*_) >> jwtToken
        !result.isPresent();
    }

    def "Destructive path - token expired"() {
        setup:
        setup:
        Map map = new HashMap();
        map.put("username", "validToken");

        when:
        tokenAuthService.isAuthenticated("bearer validToken");

        then:
        1 * jwtUtil.parseJWT(*_) >> { throw Mock(ExpiredJwtException) }
        thrown(AuthenticatorServiceException)
    }

    def "Destructive path - invalid JWT token provided"() {
        setup:
        setup:
        Map map = new HashMap();
        map.put("username", "validToken");

        when:
        tokenAuthService.isAuthenticated("bearer validToken");

        then:
        1 * jwtUtil.parseJWT(*_) >> { throw Mock(MalformedJwtException) }
        thrown(AuthenticatorServiceException)
    }
}
