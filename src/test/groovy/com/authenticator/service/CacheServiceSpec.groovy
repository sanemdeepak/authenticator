package com.authenticator.service

import com.authenticator.repository.CacheRepository
import com.authenticator.repository.impl.CacheRepositoryImpl
import spock.lang.Specification

/**
 * Created by sanemdeepak on 5/1/17.
 */
class CacheServiceSpec extends Specification {

    private CacheService cacheService;
    private CacheRepository cacheRepository;

    Integer MAX_TOKEN_COUNT = 10;

    def setup() {
        cacheRepository = Mock(CacheRepositoryImpl);
        cacheService = new CacheService(cacheRepository);
        cacheService.MAX_TOKEN_COUNT = MAX_TOKEN_COUNT;
    }

    def "Happy Path - setToken"() {
        when:
        def result = cacheService.setToken("token", "username")

        then:
        1 * cacheRepository.setToken(*_) >> true
        result
    }

    def "Destructive Path - setToken"() {
        when:
        def result = cacheService.setToken("token", "username")

        then:
        1 * cacheRepository.setToken(*_) >> false
        !result
    }

    def "Happy Path - unsetToken"() {
        when:
        def result = cacheService.unsetToken("token", "username")

        then:
        1 * cacheRepository.unsetToken(*_) >> true
        result
    }

    def "Destructive Path - unsetToken"() {
        when:
        def result = cacheService.unsetToken("token", "username")

        then:
        1 * cacheRepository.unsetToken(*_) >> false
        !result
    }

    def "Happy path - getTokens"() {
        setup:
        Map<String, String> map = new HashMap<>()
        map.put("key", "value")

        when:
        def result = cacheService.getTokens("username")

        then:
        1 * cacheRepository.getTokens(*_) >> Optional.of(map)
        result.get().get("key") == "value"
    }
}
