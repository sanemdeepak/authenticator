package com.authenticator.service

import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.exception.AuthenticatorServiceException
import com.authenticator.modal.User
import com.authenticator.repository.UserPermissionRepository
import com.authenticator.repository.UserRepository
import com.authenticator.repository.impl.UserPermissionRepositoryImpl
import com.authenticator.repository.impl.UserRepositoryImpl
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

import javax.validation.Validator;

/**
 * @author DEEPAK SANEM 
 * @date 5/11/17.
 */
class UserServiceSpec extends Specification {

    PasswordEncoder passwordEncoder;
    UserRepository userRepository;
    UserPermissionRepository userPermissionRepository;
    Validator validator;

    UserService userService;

    def setup() {
        passwordEncoder = Mock(PasswordEncoder);
        userRepository = Mock(UserRepositoryImpl);
        userPermissionRepository = Mock(UserPermissionRepositoryImpl);
        validator = Mock(Validator);
        userService = new UserService(userRepository, userPermissionRepository, validator);
    }

    def "Happy path - getUserByUserLinkId()"() {
        when:
        def result = userService.getUserByUserLinkId("userLinkId");

        then:
        1 * userRepository.findUserByUserLinkId(*_) >> Optional.of(new User())
        result.isPresent()
    }

    def "Destructive path - getUserByUserLinkId() {Cannot find user}"() {
        when:
        def result = userService.getUserByUserLinkId("userLinkId");

        then:
        1 * userRepository.findUserByUserLinkId(*_) >> {
            throw new AuthenticatorResourceException(new EmptyResultDataAccessException(1));
        }
        !result.isPresent()
    }

    def "Destructive path - getUserByUserLinkId() {throws exception}"() {
        when:
        def result = userService.getUserByUserLinkId("userLinkId");

        then:
        1 * userRepository.findUserByUserLinkId(*_) >> {
            throw new AuthenticatorResourceException(new Exception("exp"));
        }
        thrown(AuthenticatorServiceException)
    }
}
