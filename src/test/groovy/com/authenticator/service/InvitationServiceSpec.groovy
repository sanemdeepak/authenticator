package com.authenticator.service

import com.authenticator.exception.AuthenticatorResourceException
import com.authenticator.exception.AuthenticatorServiceException
import com.authenticator.modal.Invitation
import com.authenticator.modal.User
import com.authenticator.repository.InvitationRepository
import com.authenticator.repository.UserRepository
import com.authenticator.repository.impl.InvitationRepositoryImpl
import com.authenticator.repository.impl.UserRepositoryImpl
import org.springframework.dao.EmptyResultDataAccessException
import spock.lang.Specification

import javax.validation.ConstraintViolation
import javax.validation.Validator

/**
 * Created by sanemdeepak on 5/9/17.
 */
class InvitationServiceSpec extends Specification {

    InvitationRepository invitationRepository;
    UserRepository userRepository;
    Validator validator;

    InvitationService invitationService;

    def setup() {
        invitationRepository = Mock(InvitationRepositoryImpl);
        userRepository = Mock(UserRepositoryImpl);
        validator = Mock(Validator);

        invitationService = Spy(InvitationService, constructorArgs: [invitationRepository, userRepository, validator])

        invitationService.applicationUrl = "dummyUrl";
        invitationService.applicationPort = 1234;
    }

    def "Happy path - validateUserAndGenerateInvitation()"() {
        setup:
        Set set = Mock(Set);
        set.size() >> 0
        User user = Mock(User)

        when:
        def result = invitationService.validateUserAndGenerateInvitation(user)

        then:
        1 * validator.validate(*_) >> set
        1 * invitationService.getUsernameFromContext() >> "username"
        1 * userRepository.findUserLinkIdByUsername(*_) >> Optional.of("userLinkId")
        1 * user.getUserLinkId() >> "userLinkId";
        1 * userRepository.findUserByUserLinkId(*_)
        1 * invitationRepository.createNewInvitation(*_) >> true
        result.isPresent()
    }

    def "Destructive path - invalid user"() {
        setup:
        Set set = new HashSet();
        set.add(Mock(ConstraintViolation))
        User user = Mock(User)
        when:
        def result = invitationService.validateUserAndGenerateInvitation(user)

        then:
        1 * validator.validate(*_) >> set
        thrown(AuthenticatorServiceException)
    }

    def "Destructive path - cannot find newly created user"() {
        setup:
        Set set = Mock(Set);
        set.size() >> 0
        User user = Mock(User)

        when:
        def result = invitationService.validateUserAndGenerateInvitation(user)

        then:
        1 * validator.validate(*_) >> set
        1 * invitationService.getUsernameFromContext() >> "username"
        1 * userRepository.findUserLinkIdByUsername(*_) >> { throw Mock(AuthenticatorResourceException) }
        thrown(AuthenticatorServiceException)
    }

    def "Destructive - fails to create invitation()"() {
        setup:
        Set set = Mock(Set);
        set.size() >> 0
        User user = Mock(User)

        when:
        def result = invitationService.validateUserAndGenerateInvitation(user)

        then:
        1 * validator.validate(*_) >> set
        1 * invitationService.getUsernameFromContext() >> "username"
        1 * userRepository.findUserLinkIdByUsername(*_) >> Optional.of("userLinkId")
        1 * user.getUserLinkId() >> "userLinkId";
        1 * userRepository.findUserByUserLinkId(*_)
        1 * invitationRepository.createNewInvitation(*_) >> false
        !result.isPresent()
    }

    def "Happy path - findInvitationByInvitationId()"() {
        when:
        def result = invitationService.findInvitationByInvitationId("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> Optional.of("invitationId");
        result.isPresent();
    }

    def "Destructive path - findInvitationByInvitationId() {EmptyResultDataAccessException}"() {
        when:
        def result = invitationService.findInvitationByInvitationId("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> {
            throw new AuthenticatorResourceException(new EmptyResultDataAccessException(1));
        }
        !result.isPresent();
    }

    def "Destructive path - findInvitationByInvitationId() {Exception}"() {
        when:
        def result = invitationService.findInvitationByInvitationId("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> {
            throw new AuthenticatorResourceException(new Exception("exp"));
        }
        thrown(AuthenticatorServiceException)
    }

    def "Happy path - isValidInvitation()"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.isActive() >> true

        when:
        def result = invitationService.isValidInvitation("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> Optional.of(invitation);
        result
    }

    def "Dstructive path - isValidInvitation() inactive invitation"() {
        setup:
        Invitation invitation = Mock(Invitation)
        invitation.isActive() >> false

        when:
        def result = invitationService.isValidInvitation("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> Optional.of(invitation);
        !result
    }

    def "Dstructive path - isValidInvitation() {EmptyResultDataAccessException}"() {
        when:
        def result = invitationService.isValidInvitation("invitationId");
        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> {
            throw new AuthenticatorResourceException(new EmptyResultDataAccessException(1));
        }
        !result
    }

    def "Dstructive path - isValidInvitation() {Exception}"() {
        when:
        def result = invitationService.isValidInvitation("invitationId");

        then:
        1 * invitationRepository.findInvitationByInvitationId(*_) >> {
            throw new AuthenticatorResourceException(new Exception("exp"));
        }
        thrown(AuthenticatorServiceException)
    }

    def "Happy path - claimInvitation()"() {
        when:
        def result = invitationService.claimInvitation("invitationId");

        then:
        1 * invitationRepository.claimInvitation(*_) >> true
        result
    }

    def "Destructive path - claimInvitation() update failed"() {
        when:
        def result = invitationService.claimInvitation("invitationId");

        then:
        1 * invitationRepository.claimInvitation(*_) >> false
        !result
    }

    def "Destructive path - claimInvitation() Exception thrown"() {
        when:
        def result = invitationService.claimInvitation("invitationId");

        then:
        1 * invitationRepository.claimInvitation(*_) >> {
            throw new AuthenticatorResourceException(new Exception("exp"));
        }
        thrown(AuthenticatorServiceException)
    }


    def "Happy path - rollbackInvitationClaim()"() {
        when:
        def result = invitationService.rollbackInvitationClaim("invitationId");

        then:
        1 * invitationRepository.rollbackInvitationClaim(*_) >> true
        result
    }

    def "Destructive path - rollbackInvitationClaim() update failed"() {
        when:
        def result = invitationService.rollbackInvitationClaim("invitationId");

        then:
        1 * invitationRepository.rollbackInvitationClaim(*_) >> false
        !result
    }

    def "Destructive path - rollbackInvitationClaim() Exception thrown"() {
        when:
        def result = invitationService.rollbackInvitationClaim("invitationId");

        then:
        1 * invitationRepository.rollbackInvitationClaim(*_) >> {
            throw new AuthenticatorResourceException(new Exception("exp"));
        }
        thrown(AuthenticatorServiceException)
    }
}
