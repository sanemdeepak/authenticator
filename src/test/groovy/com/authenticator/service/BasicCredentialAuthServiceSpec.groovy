package com.authenticator.service

import com.authenticator.exception.AuthenticatorException
import com.authenticator.repository.UserRepository
import com.authenticator.repository.impl.UserRepositoryImpl
import org.springframework.dao.DataAccessException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

/**
 * Created by sanemdeepak on 5/1/17.
 */
class BasicCredentialAuthServiceSpec extends Specification {

    UserRepository userRepository;

    BasicCredentialAuthService basicCredentialAuthService;

    PasswordEncoder passwordEncoder;

    String validBasicAuthHeader = "Basic c2FuZW1kZWVwYWs6dGVzdHBhc3N3b3Jk"

    def setup() {
        userRepository = Mock(UserRepositoryImpl);
        basicCredentialAuthService = new BasicCredentialAuthService(userRepository);
        passwordEncoder = Mock(BCryptPasswordEncoder);
        basicCredentialAuthService.passwordEncoder = passwordEncoder;
    }

    def "Happy path - isAuthenticated"() {
        when:
        Optional<String> username = basicCredentialAuthService.isAuthenticated(validBasicAuthHeader);

        then:
        1 * userRepository.findPasswordByUsername(*_) >> Optional.of("")
        1 * passwordEncoder.matches(*_) >> true
        username.isPresent()
    }

    def "Happy path - failed to authenticate"() {
        when:
        Optional<String> username = basicCredentialAuthService.isAuthenticated(validBasicAuthHeader);

        then:
        1 * userRepository.findPasswordByUsername(*_) >> Optional.of("")
        1 * passwordEncoder.matches(*_) >> false
        !username.isPresent()
    }

    def "Destructive path[Test - 1] empty header"() {
        when:
        Optional<String> username = basicCredentialAuthService.isAuthenticated("");

        then:
        0 * userRepository.findPasswordByUsername(*_)
        0 * passwordEncoder.matches(*_)
        !username.isPresent()
    }

    def "Destructive path[Test - 2] Any exception is wrapped and re-thrown"() {
        when:
        basicCredentialAuthService.isAuthenticated(validBasicAuthHeader);

        then:
        1 * userRepository.findPasswordByUsername(*_) >> { throw Mock(DataAccessException) }
        thrown(AuthenticatorException)
    }
}
