-- Create syntax for TABLE 'Auth_Invitation'
CREATE TABLE `Auth_Invitation` (
  `invitation_id` varchar(50) NOT NULL,
  `user_link_id` varchar(50) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `expiry_timestamp` timestamp NOT NULL,
  `claimed_timestamp` timestamp NULL DEFAULT NULL,
  `created_by_user_link_id` varchar(50) NOT NULL,
  PRIMARY KEY (`invitation_id`),
  KEY `user_link_id` (`user_link_id`),
  KEY `created_by_user_link_id` (`created_by_user_link_id`),
  CONSTRAINT `Auth_Invitation_ibfk_1` FOREIGN KEY (`user_link_id`) REFERENCES `Auth_User` (`user_link_id`),
  CONSTRAINT `Auth_Invitation_ibfk_2` FOREIGN KEY (`created_by_user_link_id`) REFERENCES `Auth_User` (`user_link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'Auth_Organization'
CREATE TABLE `Auth_Organization` (
  `org_id` varchar(50) NOT NULL,
  `org_name` varchar(100) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'Auth_Permission'
CREATE TABLE `Auth_Permission` (
  `permission_id` varchar(50) NOT NULL,
  `org_id` varchar(100) DEFAULT NULL,
  `permission` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `org_id` (`org_id`),
  CONSTRAINT `Auth_Permission_ibfk_1` FOREIGN KEY (`org_id`) REFERENCES `Auth_Organization` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'Auth_User'
CREATE TABLE `Auth_User` (
  `user_link_id` varchar(50) NOT NULL,
  `user_name` varchar(15) DEFAULT NULL,
  `password` varchar(25) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone_no` varchar(12) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `org_id` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `last_login_timestamp` timestamp NULL DEFAULT NULL,
  `last_logout_timestamp` timestamp NULL DEFAULT NULL,
  `last_password_change_timestamp` timestamp NULL DEFAULT NULL,
  `last_token_issued_timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_link_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `org_id` (`org_id`),
  CONSTRAINT `Auth_User_ibfk_1` FOREIGN KEY (`org_id`) REFERENCES `Auth_Organization` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'Auth_User_Permission'
CREATE TABLE `Auth_User_Permission` (
  `permission_id` varchar(50) DEFAULT NULL,
  `user_link_id` varchar(50) DEFAULT NULL,
  `permission` varchar(150) DEFAULT NULL,
  KEY `permission_id` (`permission_id`),
  KEY `user_link_id` (`user_link_id`),
  CONSTRAINT `Auth_User_Permission_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `Auth_Permission` (`permission_id`),
  CONSTRAINT `Auth_User_Permission_ibfk_2` FOREIGN KEY (`user_link_id`) REFERENCES `Auth_User` (`user_link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE Auth_Password_Recovery(
recovery_token VARCHAR(100) NOT NULL,
user_name VARCHAR(15) NOT NULL,
try_count INT DEFAULT 0,
token_expiry TIMESTAMP NOT NULL,
FOREIGN KEY(user_name) REFERENCES Auth_User(user_name));