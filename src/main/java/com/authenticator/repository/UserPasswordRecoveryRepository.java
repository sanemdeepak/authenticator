package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.PasswordRecoveryToken;

import java.util.Optional;

/**
 * Created by sanemdeepak on 5/25/17.
 */
public interface UserPasswordRecoveryRepository {

    public Optional<PasswordRecoveryToken> addNewRecoveryToken(PasswordRecoveryToken passwordRecoveryToken) throws AuthenticatorResourceException;

    public Optional<PasswordRecoveryToken> findRecoveryTokenByTokenKey(String recoveryTokenKey) throws AuthenticatorResourceException;

    public Boolean deactivateRecoveryToken(String recoveryTokenKey) throws AuthenticatorResourceException;

    public Boolean updateTryCountForRecoveryKey(String recoveryTokenKey, Integer tryCount) throws AuthenticatorResourceException;
}
