package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.User;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/8/17.
 */
public interface UserRepository {

    public Optional<User> createUser(User user) throws AuthenticatorResourceException;

    public Optional<User> findUserByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public Optional<User> findUserByUsername(String username) throws AuthenticatorResourceException;

    public Optional<String> findPasswordByUsername(String username) throws AuthenticatorResourceException;

    public Optional<String> findPasswordByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public Optional<String> findUserLinkIdByUsername(String username) throws AuthenticatorResourceException;

    public Optional<String> findUsernameByEmail(String email) throws AuthenticatorResourceException;

    public Optional<String> findOrgIdByUsername(String username) throws AuthenticatorResourceException;

    public Optional<User> updateUserDetails(User user, String userLinkId) throws AuthenticatorResourceException;

    public boolean updateUserPassword(String userLinkId, String password) throws AuthenticatorResourceException;

    public boolean updateUserUsername(String userLinkId, String username) throws AuthenticatorResourceException;

    public boolean activateUser(String userLinkId) throws AuthenticatorResourceException;

    public boolean deactivateUser(String userLinkId) throws AuthenticatorResourceException;

    public Optional<List<User>> findAllUsersByOrganizationId(String orgId) throws AuthenticatorResourceException;

    public Optional<Timestamp> getLastLoginTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public Optional<Timestamp> getLastLogoutTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public Optional<Timestamp> getLastTokenIssuedTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public boolean updateLastLoginTimeStampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException;

    public boolean updateLastLogoutTimestampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException;

    public boolean updateLastTokenIssuedTimestampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException;

    public boolean deleteUserByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public boolean markUserDeleted(String userLinkId) throws AuthenticatorResourceException;

}
