package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Invitation;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/8/17.
 */
public interface InvitationRepository {

    public Optional<Invitation> findInvitationByInvitationId(String invitationId) throws AuthenticatorResourceException;

    public boolean createNewInvitation(Invitation invitation) throws AuthenticatorResourceException;

    public boolean invalidateInvitationByInvitationId(String invitationId) throws AuthenticatorResourceException;

    public boolean claimInvitation(String invitationId, Timestamp timestamp) throws AuthenticatorResourceException;

    public boolean rollbackInvitationClaim(String invitationId) throws AuthenticatorResourceException;
}
