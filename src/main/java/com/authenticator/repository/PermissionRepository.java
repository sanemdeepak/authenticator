package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Permission;

import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/8/17.
 */
public interface PermissionRepository {

    public Optional<List<Permission>> findAllPermissionsForOrganization(String orgId) throws AuthenticatorResourceException;

    public Optional<Permission> findPermissionById(String permissionId) throws AuthenticatorResourceException;

    public boolean createPermissionForOrganization(String permission, String orgId) throws AuthenticatorResourceException;

    public boolean createPermissionsForOrganization(List<String> permissions, String orgId) throws AuthenticatorResourceException;
}
