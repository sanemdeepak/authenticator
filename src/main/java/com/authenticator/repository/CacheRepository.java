package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorException;
import com.authenticator.exception.AuthenticatorResourceException;

import java.util.Map;
import java.util.Optional;

/**
 * Created by sanemdeepak on 4/1/17.
 */
public interface CacheRepository {

    public boolean setToken(String jwtToken, String username) throws AuthenticatorResourceException;

    public Optional<Map<String, String>> getTokens(String username) throws AuthenticatorResourceException;

    public boolean unsetToken(String tokenId, String username) throws AuthenticatorResourceException;
}
