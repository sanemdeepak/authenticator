package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.UserPermission;

import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/8/17.
 */
public interface UserPermissionRepository {

    public Optional<List<UserPermission>> findAllUserPermissionsByUserLinkId(String userLinkId) throws AuthenticatorResourceException;

    public boolean addNewUserPermission(UserPermission userPermission) throws AuthenticatorResourceException;

    public boolean addAllUserPermissions(List<UserPermission> userPermissions) throws AuthenticatorResourceException;

    public boolean activateUserPermission(String permission, String userLinkId) throws AuthenticatorResourceException;

    public boolean deactivateUserPermission(String permission, String userLinkId) throws AuthenticatorResourceException;

}
