package com.authenticator.repository;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Organization;

import java.util.Optional;

/**
 * Created by sanemdeepak on 3/8/17.
 */
public interface OrganizationRepository {

    public Optional<Organization> findOrganizationByOrgId(String orgId) throws AuthenticatorResourceException;

    public boolean createNewOrganization(Organization organization) throws AuthenticatorResourceException;

    public boolean updateOrganization(Organization organization) throws AuthenticatorResourceException;

    public boolean deactivateOrganizationById(String orgId) throws AuthenticatorResourceException;

    public boolean activateOrganizationById(String orgId) throws AuthenticatorResourceException;

}
