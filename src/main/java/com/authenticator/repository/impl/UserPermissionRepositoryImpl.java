package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.UserPermission;
import com.authenticator.repository.UserPermissionRepository;
import com.authenticator.utilities.rowMappers.UserPermissionRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/18/17.
 */

@Repository
public class UserPermissionRepositoryImpl implements UserPermissionRepository {

    private final Logger logger = LoggerFactory.getLogger(UserPermissionRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final UserPermissionRowMapper userPermissionRowMapper;

    @Autowired
    protected UserPermissionRepositoryImpl(JdbcTemplate jdbcTemplate, UserPermissionRowMapper userPermissionRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userPermissionRowMapper = userPermissionRowMapper;
    }

    private String findAllUserPermissionsByUserLinkIdQuery = "SELECT * " +
            "FROM Auth_User_Permission" +
            " WHERE user_link_id = ?";

    private String addNewUserPermissionQuery = "INSERT INTO Auth_User_Permission" +
            "(permission_id, user_link_id, permission)" +
            "VALUES(?, ?, ?)";

    private String activateUserPermissionQuery = "UPDATE Auth_User_Permission" +
            " SET active = true" +
            " WHERE user_link_id = ? AND permission = ?";

    private String deactivateUserPermissionQuery = "UPDATE Auth_User_Permission" +
            " SET active = false" +
            " WHERE user_link_id = ? AND permission = ?";


    @Override
    public Optional<List<UserPermission>> findAllUserPermissionsByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(jdbcTemplate.query(findAllUserPermissionsByUserLinkIdQuery, userPermissionRowMapper, userLinkId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding all user permissions: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean addNewUserPermission(UserPermission userPermission) {

        if (StringUtils.isEmpty(userPermission.getPermissionId()) ||
                StringUtils.isEmpty(userPermission.getUserLinkId()) ||
                StringUtils.isEmpty(userPermission.getPermission())) {
            return false;
        }

        try {
            return jdbcTemplate.update(addNewUserPermissionQuery, userPermission.getPermissionId(), userPermission.getUserLinkId(),
                    userPermission.getPermission()) >= 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while adding new UserPermission: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean addAllUserPermissions(List<UserPermission> userPermissions) {

        if (userPermissions.size() < 1) {
            return false;
        }

        try {
            for (UserPermission userPermission : userPermissions) {
                if (!addNewUserPermission(userPermission)) {
                    return false;
                }
            }

            return true;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while adding new UserPermission: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean activateUserPermission(String permission, String userLinkId) {

        if (StringUtils.isEmpty(permission) || StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(activateUserPermissionQuery, userLinkId, permission) >= 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while adding new UserPermission: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean deactivateUserPermission(String permission, String userLinkId) {

        if (StringUtils.isEmpty(permission) || StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(deactivateUserPermissionQuery, userLinkId, permission) >= 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while adding new UserPermission: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }
}
