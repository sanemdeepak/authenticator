package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Organization;
import com.authenticator.repository.OrganizationRepository;
import com.authenticator.utilities.rowMappers.OrganizationRowMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by sanemdeepak on 3/15/17.
 */

@Repository
public class OrganizationRepositoryImpl implements OrganizationRepository {

    private final Logger logger = LoggerFactory.getLogger(OrganizationRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final OrganizationRowMapper organizationRowMapper;

    @Autowired
    protected OrganizationRepositoryImpl(JdbcTemplate jdbcTemplate, OrganizationRowMapper organizationRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.organizationRowMapper = organizationRowMapper;
    }

    private final String findOrganizationByIdQuery = "SELECT org_id, org_name, address, phone_np, email, created_on " +
            "FROM Auth_Organization";

    private final String createNewOrganizationQuery = "INSERT INTO Auth_Organization(org_id, org_name, address, phone_no, email, createdOn) " +
            "VALUES(?, ?, ?, ?, ?, ?)";

    private final String updateOrganizationQuery = "UPDATE Auth_Organization " +
            "SET" +
            "org_name = ?" +
            ",address = ?" +
            ",phone_no = ?" +
            ",email = ?" +
            "WHERE" +
            "org_id = ?";

    private final String deactivateOrganizationQuery = "UPDATE Auth_Organization" +
            "SET" +
            "active = false" +
            "WHERE" +
            "org_id = ?";

    private final String activateOrganizationQuery = "UPDATE Auth_Organization" +
            "SET" +
            "active = true" +
            "WHERE" +
            "org_id = ?";


    @Override
    public Optional<Organization> findOrganizationByOrgId(String orgId) throws AuthenticatorResourceException {

        try {
            Organization organization = jdbcTemplate.queryForObject(findOrganizationByIdQuery, organizationRowMapper, orgId);
            return Optional.ofNullable(organization);
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding organization: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean createNewOrganization(Organization organization) {
        try {
            return jdbcTemplate.update(createNewOrganizationQuery,
                    organization.getOrgId(),
                    organization.getOrgName(),
                    organization.getAddress(),
                    organization.getPhoneNumber(),
                    organization.getEmail(),
                    organization.getCreatedOn()) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while creating organization: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean updateOrganization(Organization organization) throws AuthenticatorResourceException {
        try {
            return jdbcTemplate.update(updateOrganizationQuery,
                    organization.getOrgName(),
                    organization.getAddress(),
                    organization.getPhoneNumber(),
                    organization.getEmail(),

                    organization.getOrgId()) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while updating organization: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean deactivateOrganizationById(String orgId) throws AuthenticatorResourceException {

        try {
            return jdbcTemplate.update(deactivateOrganizationQuery, orgId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while deactivating organization: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean activateOrganizationById(String orgId) throws AuthenticatorResourceException {

        try {
            return jdbcTemplate.update(activateOrganizationQuery, orgId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while activating organization: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }
}
