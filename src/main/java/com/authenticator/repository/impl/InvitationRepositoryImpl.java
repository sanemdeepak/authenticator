package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Invitation;
import com.authenticator.repository.InvitationRepository;
import com.authenticator.utilities.rowMappers.InvitationRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/12/17.
 */
@Repository
public class InvitationRepositoryImpl implements InvitationRepository {

    private final Logger logger = LoggerFactory.getLogger(InvitationRepositoryImpl.class);

    private JdbcTemplate jdbcTemplate;
    private final InvitationRowMapper invitationRowMapper;

    @Autowired
    protected InvitationRepositoryImpl(JdbcTemplate jdbcTemplate, InvitationRowMapper invitationRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.invitationRowMapper = invitationRowMapper;
    }

    protected final String findInvitationByIdQuery = "SELECT invitation_id, user_link_id, active, expiry_timestamp, " +
            "claimed_timestamp, created_by_user_link_id " +
            "FROM Auth_Invitation " +
            "WHERE invitation_id = ?";

    protected final String findUserLinkIdByInvitationId = "SELECT user_link_id" +
            " FROM Auth_Invitation" +
            " WHERE invitation_id = ?";

    protected final String createNewInvitationQuery = "INSERT INTO Auth_Invitation" +
            "(invitation_id, " +
            "user_link_id, " +
            "expiry_timestamp, " +
            "created_by_user_link_id) " +
            "VALUES(?,?,?,?)";

    protected final String invalidateInvitationQuery = "UPDATE Auth_Invitation" +
            " SET active = false" +
            " WHERE invitation_id = ?";

    protected final String claimInvitationQuery = "UPDATE Auth_Invitation" +
            " SET active = false, claimed_timestamp = ?" +
            " WHERE invitation_id = ?";

    protected final String rollbackInvitationClaimQuery = "UPDATE Auth_Invitation" +
            " SET active = true, claimed_timestamp = NULL" +
            " WHERE invitation_id = ?";


    @Override
    public Optional<Invitation> findInvitationByInvitationId(String invitationId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(invitationId)) {
            return Optional.empty();
        }
        try {
            Invitation invitation = jdbcTemplate.queryForObject(findInvitationByIdQuery, invitationRowMapper, invitationId);
            return Optional.ofNullable(invitation);
        } catch (DataAccessException dae) {
            logger.warn("Exception caught querying for invitation: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean createNewInvitation(Invitation invitation) {

        if (invitation == null ||
                StringUtils.isEmpty(invitation.getInvitationId()) ||
                StringUtils.isEmpty(invitation.getUserLinkId()) ||
                StringUtils.isEmpty(invitation.getCreatedByUserLinkId()) ||
                invitation.getExpiryTimestamp() == null ||
                invitation.getExpiryTimestamp().before(Timestamp.from(Instant.now()))) {

            return false;
        }

        try {
            return jdbcTemplate.update(createNewInvitationQuery,
                    invitation.getInvitationId(),
                    invitation.getUserLinkId(),
                    invitation.getExpiryTimestamp(),
                    invitation.getCreatedByUserLinkId()) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught creating invitation: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean invalidateInvitationByInvitationId(String invitationId) {
        if (StringUtils.isEmpty(invitationId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(invalidateInvitationQuery, invitationId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught invalidating invitation: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean claimInvitation(String invitationId, Timestamp timestamp) {
        if (StringUtils.isEmpty(invitationId) || timestamp == null) {
            return false;
        }

        try {
            return jdbcTemplate.update(claimInvitationQuery, timestamp, invitationId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught claiming invitation: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean rollbackInvitationClaim(String invitationId) {
        if (StringUtils.isEmpty(invitationId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(rollbackInvitationClaimQuery, invitationId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught rolling back invitation: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }
}
