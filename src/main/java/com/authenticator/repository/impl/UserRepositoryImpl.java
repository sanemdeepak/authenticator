package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.User;
import com.authenticator.repository.UserRepository;
import com.authenticator.utilities.rowMappers.UserRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 3/27/17.
 */

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;

    @Autowired
    protected UserRepositoryImpl(JdbcTemplate jdbcTemplate, UserRowMapper userRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
    }

    private final String createUserQuery = "INSERT INTO Auth_User" +
            "(user_link_id, " +
            "user_name, " +
            "password, " +
            "first_name, " +
            "last_name," +
            "email, " +
            "phone_no, " +
            "address, " +
            "org_id) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private final String findUserByUserLinkIdQuery = "SELECT * " +
            "FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String findPasswordByUsernameQuery = "SELECT password" +
            " FROM Auth_User" +
            " WHERE user_name = ?";

    private final String findPasswordByUserLinkIdQuery = "SELECT password" +
            " FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String findUserLinkIdByUsernameQuery = "SELECT user_link_id" +
            " FROM Auth_User" +
            " WHERE user_name = ?";

    private final String findUsernameByEmailQuery = "SELECT user_name" +
            " FROM Auth_User" +
            " WHERE email = ?";

    private final String findOrgIdByUsernameQuery = "SELECT org_id" +
            " FROM Auth_User" +
            " WHERE user_name = ?";

    private final String updateUserDetailsQuery = "UPDATE Auth_User" +
            " SET" +
            " first_name = ?," +
            "last_name = ?," +
            "email = ?," +
            "phone_no = ?," +
            "address = ?" +
            "WHERE user_link_id = ?";

    private final String updateUserPasswordQuery = "UPDATE Auth_User" +
            " SET password = ?" +
            "WHERE user_link_id = ?";

    private final String updateUserUsernameQuery = "UPDATE Auth_User" +
            " SET user_name = ?" +
            "WHERE user_link_id = ?";

    private final String activateUserQuery = "UPDATE Auth_User" +
            " SET active = true" +
            " WHERE user_link_id = ?";

    private final String deactivateUserQuery = "UPDATE Auth_User" +
            " SET active = false" +
            " WHERE user_link_id = ?";

    private final String findAllUsersByOrganizationIdQuery = "SELECT *" +
            "FROM Auth_User" +
            " WHERE org_id = ?";

    private final String getLastLoginTimestampByUserLinkIdQuery = "SELECT last_login_timestamp" +
            " FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String getLastLogoutTimestampByUserLinkIdQuery = "SELECT last_logout_timestamp" +
            " FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String getLastTokenIssuedTimestampByUserLinkIdQuery = "SELECT last_token_issued_timestamp" +
            " FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String updateLastLoginTimeStampOfUserLinkIdQuery = "UPDATE Auth_User" +
            " SET last_login_timestamp = ?" +
            "WHERE user_link_id = ?";

    private final String updateLastLogoutTimestampOfUserLinkIdQuery = "UPDATE Auth_User" +
            " SET last_logout_timestamp = ?" +
            "WHERE user_link_id = ?";

    private final String updateLastTokenIssuedTimestampOfUserLinkIdQuery = "UPDATE Auth_User" +
            " SET last_token_issued_timestamp = ?" +
            "WHERE user_link_id = ?";


    private final String deleteUserByUserLinkIdQuery = "DELETE FROM Auth_User" +
            " WHERE user_link_id = ?";

    private final String markUserDeletedQuery = "UPDATE Auth_User" +
            " SET deleted = true" +
            " WHERE user_link_id = ?";


    @Override
    public Optional<User> createUser(User user) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(user.getUserLinkId())
                || StringUtils.isEmpty(user.getUsername())
                || StringUtils.isEmpty(user.getPassword())
                || StringUtils.isEmpty(user.getFirstName())
                || StringUtils.isEmpty(user.getLastName())
                || StringUtils.isEmpty(user.getEmail())
                || StringUtils.isEmpty(user.getPhoneNumber())
                || StringUtils.isEmpty(user.getAddress())
                || StringUtils.isEmpty(user.getOrgId())
                ) {
            logger.info("Could not create user: {}", user.toString());
            return Optional.empty();
        }
        try {
            int result = jdbcTemplate.update(createUserQuery,
                    user.getUserLinkId(),
                    user.getUsername(),
                    user.getPassword(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getEmail(),
                    user.getPhoneNumber(),
                    user.getAddress(),
                    user.getOrgId());

            if (result != 1) {
                logger.error("Could not create user: {} JDBC update failed", user.toString());
                throw new AuthenticatorResourceException("Error creating user: " + user);
            }

            return this.findUserByUserLinkId(user.getUserLinkId());
        } catch (DataAccessException dae) {
            logger.error("Could not create user: {} JDBC update failed: {} ", user.toString(), ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<User> findUserByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("Cannot find user with empty userLinkId");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findUserByUserLinkIdQuery, userRowMapper, userLinkId));
        } catch (DataAccessException dae) {
            logger.warn("Exception caught while finding user by id: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<User> findUserByUsername(String username) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(username)) {
            logger.info("Cannot find user with empty username");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findUserByUserLinkIdQuery, userRowMapper, username));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding user by username: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<String> findPasswordByUsername(String username) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(username)) {
            logger.info("Cannot find password with empty username");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findPasswordByUsernameQuery, String.class, username));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding password by username: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<String> findPasswordByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("Cannot find password with empty userLinkId");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findPasswordByUserLinkIdQuery, String.class, userLinkId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding password by id: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<String> findUserLinkIdByUsername(String username) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(username)) {
            logger.info("Cannot find userLinkId with empty username");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findUserLinkIdByUsernameQuery, String.class, username));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding user_link_id by username: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<String> findUsernameByEmail(String email) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(email)) {
            logger.info("Cannot find username with empty email");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findUsernameByEmailQuery, String.class, email));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding username by email: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<String> findOrgIdByUsername(String username) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(username)) {
            logger.info("Cannot find OrgId with empty username");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findOrgIdByUsernameQuery, String.class, username));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding OrgId by username: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<User> updateUserDetails(User user, String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return Optional.empty();
        }

        if (StringUtils.isEmpty(user.getFirstName())
                || StringUtils.isEmpty(user.getLastName())
                || StringUtils.isEmpty(user.getEmail())
                || StringUtils.isEmpty(user.getPhoneNumber())
                || StringUtils.isEmpty(user.getAddress())
                ) {
            logger.info("Could not update user: {}", user.toString());
            return Optional.empty();
        }

        try {
            int result = jdbcTemplate.update(updateUserDetailsQuery,
                    user.getFirstName(),
                    user.getLastName(),
                    user.getEmail(),
                    user.getPhoneNumber(),
                    user.getAddress(),

                    userLinkId);

            if (result != 1) {
                logger.error("Could not update user: {} JDBC update failed", user.toString());
                throw new AuthenticatorResourceException("Error updating user: " + user);
            }

            return findUserByUserLinkId(userLinkId);
        } catch (DataAccessException dae) {
            logger.error("Exception caught while updating user details: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean updateUserPassword(String userLinkId, String password) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId) || StringUtils.isEmpty(password)) {
            logger.info("userLinkId or password cannot be empty");
            return false;
        }
        try {
            return jdbcTemplate.update(updateUserPasswordQuery, password, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while updating password: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean updateUserUsername(String userLinkId, String username) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(userLinkId) || StringUtils.isEmpty(username)) {
            logger.info("userLinkId or username cannot be empty");
            return false;
        }
        try {
            return jdbcTemplate.update(updateUserUsernameQuery, username, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while updating username: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean activateUser(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }
        try {
            return jdbcTemplate.update(activateUserQuery, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while activating user: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean deactivateUser(String userLinkId) {
        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }
        try {
            return jdbcTemplate.update(deactivateUserQuery, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while deactivating user: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }

    }

    @Override
    public Optional<List<User>> findAllUsersByOrganizationId(String orgId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(orgId)) {
            logger.info("OrgId cannot be empty");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.query(findAllUsersByOrganizationIdQuery, userRowMapper, orgId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing findAllUsersByOrganizationId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<Timestamp> getLastLoginTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(getLastLoginTimestampByUserLinkIdQuery, Timestamp.class, userLinkId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing getLastLoginTimestampByUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }

    }

    @Override
    public Optional<Timestamp> getLastLogoutTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(getLastLogoutTimestampByUserLinkIdQuery, Timestamp.class, userLinkId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing getLastLogoutTimestampByUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<Timestamp> getLastTokenIssuedTimestampByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(getLastTokenIssuedTimestampByUserLinkIdQuery, Timestamp.class, userLinkId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing getLastTokenIssuedTimestampByUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }

    }

    @Override
    public boolean updateLastLoginTimeStampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return false;
        }
        if (timestamp == null) {
            logger.info("timestamp cannot be null");
            return false;
        }

        try {
            return jdbcTemplate.update(updateLastLoginTimeStampOfUserLinkIdQuery, timestamp, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing updateLastLoginTimeStampOfUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean updateLastLogoutTimestampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return false;
        }
        if (timestamp == null) {
            logger.info("timestamp cannot be null");
            return false;
        }

        try {

            return jdbcTemplate.update(updateLastLogoutTimestampOfUserLinkIdQuery, timestamp, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing updateLastLogoutTimestampOfUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean updateLastTokenIssuedTimestampOfUserLinkId(Timestamp timestamp, String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            logger.info("userLinkId cannot be empty");
            return false;
        }
        if (timestamp == null) {
            logger.info("timestamp cannot be null");
            return false;
        }

        try {
            return jdbcTemplate.update(updateLastTokenIssuedTimestampOfUserLinkIdQuery, timestamp, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing updateLastTokenIssuedTimestampOfUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }


    /*
    This method should be used only when rolling back
     */
    @Override
    public boolean deleteUserByUserLinkId(String userLinkId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(deleteUserByUserLinkIdQuery, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing deleteUserByUserLinkId: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }


    }

    @Override
    public boolean markUserDeleted(String userLinkId) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        try {
            return jdbcTemplate.update(markUserDeletedQuery, userLinkId) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while executing markUserDeleted: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }

    }
}
