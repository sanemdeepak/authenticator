package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.Permission;
import com.authenticator.repository.PermissionRepository;
import com.authenticator.utilities.rowMappers.PermissionRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by sanemdeepak on 3/17/17.
 */

@Repository
public class PermissionRepositoryImpl implements PermissionRepository {

    private final Logger logger = LoggerFactory.getLogger(PermissionRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final PermissionRowMapper permissionRowMapper;

    @Autowired
    protected PermissionRepositoryImpl(JdbcTemplate jdbcTemplate, PermissionRowMapper permissionRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.permissionRowMapper = permissionRowMapper;
    }

    private final String findAllPermissionForOrganizationsQuery = "SELECT * " +
            "FROM Auth_Permission" +
            " WHERE org_id = ?";

    private final String findPermissionByPermissionIdQuery = "SELECT *" +
            "FROM Auth_Permission" +
            " WHERE permission_id = ?";

    private final String createNewPermissionForOrganizationQuery = "INSERT INTO Auth_Permission" +
            "(permission_id, org_id, permission)" +
            " VALUES(?, ?, ?)";

    @Override
    public Optional<List<Permission>> findAllPermissionsForOrganization(String orgId) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(orgId)) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(jdbcTemplate.query(findAllPermissionForOrganizationsQuery, permissionRowMapper, orgId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding permissions: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<Permission> findPermissionById(String permissionId) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(permissionId)) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findPermissionByPermissionIdQuery, permissionRowMapper, permissionId));
        } catch (DataAccessException dae) {
            logger.error("Exception caught while finding permission by id: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean createPermissionForOrganization(String permission, String orgId) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(permission) || StringUtils.isEmpty(orgId)) {
            return false;
        }

        String randomPermissionId = UUID.randomUUID().toString();

        try {
            return jdbcTemplate.update(createNewPermissionForOrganizationQuery, randomPermissionId, orgId, permission) == 1;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while creating permission: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public boolean createPermissionsForOrganization(List<String> permissions, String orgId) throws AuthenticatorResourceException {

        if ((permissions.size() < 1) || StringUtils.isEmpty(orgId)) {
            return false;
        }

        try {
            for (String permission : permissions) {
                if (!createPermissionForOrganization(permission, orgId)) {
                    return false;
                }
            }
            return true;
        } catch (DataAccessException dae) {
            logger.error("Exception caught while creating permissions: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }
}
