package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.modal.PasswordRecoveryToken;
import com.authenticator.repository.UserPasswordRecoveryRepository;
import com.authenticator.utilities.rowMappers.PasswordRecoveryTokenRowMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by sanemdeepak on 5/25/17.
 */
@Repository
public class UserPasswordRecoveryRepoImpl implements UserPasswordRecoveryRepository {

    private final Logger logger = LoggerFactory.getLogger(UserPasswordRecoveryRepoImpl.class);

    private JdbcTemplate jdbcTemplate;

    private PasswordRecoveryTokenRowMapper passwordRecoveryTokenRowMapper;

    private final String findRecoveryTokenByRecoveryTokenKeyQuery = "SELECT " +
            "recovery_token_key, " +
            "user_name, " +
            "try_count, " +
            "token_expiry, " +
            "active" +
            " FROM Auth_Password_Recovery" +
            " WHERE recovery_token_key = ?";

    private final String deactivateRecoveryTokenQuery = "UPDATE Auth_Password_Recovery" +
            " SET active = false" +
            " WHERE recovery_token_key = ?";

    private final String updateTryCountForRecoveryKey = "UPDATE Auth_Password_Recovery" +
            " SET try_count = ?" +
            " WHERE recovery_token_key = ?";

    private final String addNewRecoveryToken = "INSERT INTO Auth_Password_Recovery" +
            "(recovery_token_key, user_name, try_count, token_expiry, active)" +
            " VALUES(?, ?, ?, ?, ?)";


    @Autowired
    protected UserPasswordRecoveryRepoImpl(JdbcTemplate jdbcTemplate, PasswordRecoveryTokenRowMapper passwordRecoveryTokenRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordRecoveryTokenRowMapper = passwordRecoveryTokenRowMapper;
    }

    @Override
    public Optional<PasswordRecoveryToken> addNewRecoveryToken(PasswordRecoveryToken passwordRecoveryToken) {

        if (StringUtils.isEmpty(passwordRecoveryToken.getRecoveryTokenKey()) ||
                StringUtils.isEmpty(passwordRecoveryToken.getUserName()) ||
                StringUtils.isEmpty(passwordRecoveryToken.getTokenExpiry().toString())) {

            return Optional.empty();

        }

        try {
            int result = jdbcTemplate.update(addNewRecoveryToken,
                    passwordRecoveryToken.getRecoveryTokenKey(),
                    passwordRecoveryToken.getUserName(),
                    0,
                    passwordRecoveryToken.getTokenExpiry(),
                    1);

            if (result != 1) {
                logger.error("Could not create recovery token: {} JDBC update failed", passwordRecoveryToken.toString());
                throw new AuthenticatorResourceException("Error creating recovery token: " + passwordRecoveryToken);
            }

            return findRecoveryTokenByTokenKey(passwordRecoveryToken.getRecoveryTokenKey());
        } catch (DataAccessException dae) {
            logger.error("Could not recovery token: {} JDBC update failed: {} ", passwordRecoveryToken.toString(), ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Optional<PasswordRecoveryToken> findRecoveryTokenByTokenKey(String recoveryTokenKey) {

        if (StringUtils.isEmpty(recoveryTokenKey)) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(findRecoveryTokenByRecoveryTokenKeyQuery, passwordRecoveryTokenRowMapper, recoveryTokenKey));
        } catch (DataAccessException dae) {
            logger.warn("Exception caught while executing findRecoveryTokenByTokenKey: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Boolean deactivateRecoveryToken(String recoveryTokenKey) {

        if (StringUtils.isEmpty(recoveryTokenKey)) {
            return Boolean.FALSE;
        }

        try {
            return jdbcTemplate.update(deactivateRecoveryTokenQuery, recoveryTokenKey) == 1;
        } catch (DataAccessException dae) {
            logger.warn("Exception caught while executing findRecoveryTokenByTokenKey: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }

    @Override
    public Boolean updateTryCountForRecoveryKey(String recoveryTokenKey, Integer tryCount) {

        if (StringUtils.isEmpty(recoveryTokenKey)) {
            return Boolean.FALSE;
        }

        try {
            return jdbcTemplate.update(updateTryCountForRecoveryKey, tryCount, recoveryTokenKey) == 1;
        } catch (DataAccessException dae) {
            logger.warn("Exception caught while executing findRecoveryTokenByTokenKey: {}", ExceptionUtils.getStackTrace(dae));
            throw new AuthenticatorResourceException(dae);
        }
    }
}
