package com.authenticator.repository.impl;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.repository.CacheRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

/**
 * Created by sanemdeepak on 4/1/17.
 */

@Repository
public class CacheRepositoryImpl implements CacheRepository {

    private final Logger logger = LoggerFactory.getLogger(CacheRepositoryImpl.class);

    private JedisPool jedisPool;

    private ObjectMapper objectMapper;

    @Value("${jwt.max.token.count}")
    protected Integer MAX_TOKEN_COUNT;

    @Autowired
    protected CacheRepositoryImpl(JedisPool jedisPool, ObjectMapper objectMapper) {
        this.jedisPool = jedisPool;
        this.objectMapper = objectMapper;
    }

    @Override
    public boolean setToken(String jwtToken, String username) throws AuthenticatorResourceException {

        String now = Timestamp.from(Instant.now()).toString();

        if (StringUtils.isEmpty(jwtToken)
                || StringUtils.isEmpty(username)) {
            return false;
        }

        try (Jedis jedis = jedisPool.getResource()) {
            long result = jedis.hset(username, now, jwtToken);
            logger.info("Redis Response: " + result);
            return (result == 0 || result == 1);
        } catch (Exception e) {
            logger.warn("Exception caught while setting token: {}", ExceptionUtils.getStackTrace(e));
            throw new AuthenticatorResourceException(e);
        }
    }

    @Override
    public Optional<Map<String, String>> getTokens(String username) throws AuthenticatorResourceException {

        if (StringUtils.isEmpty(username)) {
            return Optional.empty();
        }
        try (Jedis jedis = jedisPool.getResource()) {
            Optional<Map<String, String>> result = Optional.ofNullable(jedis.hgetAll(username));
            return result;
        } catch (Exception e) {
            logger.warn("Exception caught while setting token: {}", ExceptionUtils.getStackTrace(e));
            throw new AuthenticatorResourceException(e);
        }
    }

    @Override
    public boolean unsetToken(String tokenId, String username) throws AuthenticatorResourceException {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(tokenId)) {
            return false;
        }
        try (Jedis jedis = jedisPool.getResource()) {
            long result = jedis.hdel(username, tokenId);
            return result == 1;
        } catch (Exception e) {
            logger.warn("Exception caught while setting token: {}", ExceptionUtils.getStackTrace(e));
            throw new AuthenticatorResourceException(e);
        }
    }
}
