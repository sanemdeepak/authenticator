package com.authenticator.exception;

/**
 * Created by sanemdeepak on 3/29/17.
 */
public class AuthenticatorException extends RuntimeException {

    private AuthenticatorException() {
    }

    public AuthenticatorException(String cause) {
        super(cause);
    }

    public AuthenticatorException(Throwable cause) {
        super(cause);
    }

}
