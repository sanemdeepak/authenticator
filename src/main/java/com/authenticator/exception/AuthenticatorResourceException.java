package com.authenticator.exception;

/**
 * Created by sanemdeepak on 5/2/17.
 */
public class AuthenticatorResourceException extends AuthenticatorException {

    public AuthenticatorResourceException(String cause) {
        super(cause);
    }

    public AuthenticatorResourceException(Throwable cause) {
        super(cause);
    }
}
