package com.authenticator.exception;

/**
 * Created by sanemdeepak on 5/2/17.
 */
public class AuthenticatorServiceException extends AuthenticatorException {

    public AuthenticatorServiceException(String cause) {
        super(cause);
    }

    public AuthenticatorServiceException(Throwable cause) {
        super(cause);
    }
}
