package com.authenticator.security;

import com.authenticator.security.exception.JwtTokenMissingException;
import com.authenticator.security.exception.JwtTokenValidationException;
import com.authenticator.security.model.AuthenticatedUser;
import com.authenticator.security.model.JwtAuthenticationToken;
import com.authenticator.service.BasicCredentialAuthService;
import com.authenticator.service.TokenAuthService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Used for checking the token from the request and supply the UserDetails if the token is valid
 *
 * @author pascal alma
 */
@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private final TokenAuthService tokenAuthService;
    private final BasicCredentialAuthService basicCredentialAuthService;

    @Autowired
    public JwtAuthenticationProvider(TokenAuthService tokenAuthService, BasicCredentialAuthService basicCredentialAuthService) {
        this.tokenAuthService = tokenAuthService;
        this.basicCredentialAuthService = basicCredentialAuthService;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();

        if (StringUtils.isEmpty(token)) {
            throw new JwtTokenMissingException("No Authentication header found");
        }

        if (StringUtils.startsWithIgnoreCase(token, "basic")) {

            Optional<String> maybeAuthenticatedUsername = basicCredentialAuthService.isAuthenticated(token);

            if (maybeAuthenticatedUsername.isPresent()) {
                return new AuthenticatedUser(maybeAuthenticatedUsername.get(), token, null);
            } else {
                throw new JwtTokenValidationException("Invalid credentials");
            }

        } else if (StringUtils.startsWithIgnoreCase(token, "bearer")) {

            Optional<String> maybeAuthenticatedUsername = tokenAuthService.isAuthenticated(token);

            if (maybeAuthenticatedUsername.isPresent()) {
                return new AuthenticatedUser(maybeAuthenticatedUsername.get(), token, null);
            } else {
                throw new JwtTokenValidationException("Invalid credentials");
            }

        } else {
            throw new JwtTokenValidationException("Invalid header");
        }
    }
}
