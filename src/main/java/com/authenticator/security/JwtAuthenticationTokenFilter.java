package com.authenticator.security;

import com.authenticator.security.exception.JwtTokenMissingException;
import com.authenticator.security.model.JwtAuthenticationToken;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Filter that orchestrates authentication by using supplied JWT token
 *
 * @author pascal alma
 */
public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${auth.ignore.urls}")
    private String ignoreUrls;

    public JwtAuthenticationTokenFilter() {
        super("/**");
    }

    /**
     * Attempt to authenticate request - basically just pass over to another method to authenticate request headers
     */

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        Authentication authentication;

        if (!requiresAuthentication(request, response)) {
            chain.doFilter(request, response);
            return;
        }
        try {
            authentication = attemptAuthentication(request, response);
            if (authentication.isAuthenticated()) {
                successfulAuthentication(request, response, chain, authentication);
            }
        } catch (AuthenticationException exp) {
            unsuccessfulAuthentication(request, response, exp);
        } catch (Exception exp) {
            unsuccessfulAuthentication(request, response, new AuthenticationServiceException(exp.getMessage(), exp));
        }
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Set<String> urls = ignoreUrlSet();
        for (String str : urls) {
            if (request.getRequestURI().contains(str)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String header = request.getHeader(this.tokenHeader);

        if (StringUtils.isEmpty(header)) {
            throw new JwtTokenMissingException("No Authentication header found");
        }

        JwtAuthenticationToken authRequest = new JwtAuthenticationToken(header);

        return getAuthenticationManager().authenticate(authRequest);
    }

    /**
     * Make sure the rest of the filterchain is satisfied
     *
     * @param request
     * @param response
     * @param chain
     * @param authResult
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, exception);
    }

    private Set<String> ignoreUrlSet() {
        Set<String> urlSet = new HashSet<>();
        String[] urls = ignoreUrls.split(",");

        for (String str : urls) {
            urlSet.add(str.trim());
        }
        return urlSet;
    }
}