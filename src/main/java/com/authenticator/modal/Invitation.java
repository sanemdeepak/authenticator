package com.authenticator.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.sql.Timestamp;

/**
 * Created by sanemdeepak on 3/7/17.
 */
@JsonRootName(value = "invitation")
public class Invitation {

    private String invitationId;
    private String userLinkId;

    @JsonIgnore
    private boolean isActive;

    @JsonIgnore
    private Timestamp expiryTimestamp;

    @JsonIgnore
    private Timestamp claimedTimestamp;

    @JsonIgnore
    private String createdByUserLinkId;

    public String getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(String invitationId) {
        this.invitationId = invitationId;
    }

    public String getUserLinkId() {
        return userLinkId;
    }

    public void setUserLinkId(String userLinkId) {
        this.userLinkId = userLinkId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Timestamp getExpiryTimestamp() {
        return expiryTimestamp;
    }

    public void setExpiryTimestamp(Timestamp expiryTimestamp) {
        this.expiryTimestamp = expiryTimestamp;
    }

    public Timestamp getClaimedTimestamp() {
        return claimedTimestamp;
    }

    public void setClaimedTimestamp(Timestamp claimedTimestamp) {
        this.claimedTimestamp = claimedTimestamp;
    }

    public String getCreatedByUserLinkId() {
        return createdByUserLinkId;
    }

    public void setCreatedByUserLinkId(String createdByUserLinkId) {
        this.createdByUserLinkId = createdByUserLinkId;
    }

    @Override
    public String toString() {
        return "Invitation{" +
                "invitationId='" + invitationId + '\'' +
                ", userLinkId='" + userLinkId + '\'' +
                ", isActive=" + isActive +
                ", expiryTimestamp=" + expiryTimestamp +
                ", claimedTimestamp=" + claimedTimestamp +
                ", createdByUserLinkId='" + createdByUserLinkId + '\'' +
                '}';
    }
}
