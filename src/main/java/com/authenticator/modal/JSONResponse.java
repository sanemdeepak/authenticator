package com.authenticator.modal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author DEEPAK SANEM
 * @date 4/19/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONResponse {

    private Timestamp timestamp;
    private RequestStatus status;
    private Integer errorCode;
    private String data;
    private String message;

    private JSONResponse() {
    }

    public JSONResponse(Timestamp timestamp, RequestStatus requestStatus) {
        this.timestamp = timestamp;
        this.status = requestStatus;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static class JSONResponseBuilder {

        private Timestamp timestamp;
        private RequestStatus status;
        private Integer errorCode;
        private String data;
        private String message;

        private JSONResponse jsonResponse;

        private JSONResponseBuilder() {
        }

        public JSONResponseBuilder(Timestamp timestamp, RequestStatus requestStatus) {
            this.timestamp = timestamp;
            this.status = requestStatus;
        }

        public JSONResponseBuilder(RequestStatus requestStatus) {
            this.timestamp = Timestamp.from(Instant.now());
            this.status = requestStatus;
        }

        public JSONResponseBuilder withErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
            return this;
        }

        public JSONResponseBuilder withData(String data) {
            this.data = data;
            return this;
        }

        public JSONResponseBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public JSONResponse buildResponse() {
            jsonResponse = new JSONResponse(this.timestamp, this.status);
            jsonResponse.setErrorCode(errorCode);
            jsonResponse.setData(data);
            jsonResponse.setMessage(message);
            return jsonResponse;
        }

        public String buildResponseAsString() {
            jsonResponse = new JSONResponse(this.timestamp, this.status);
            jsonResponse.setErrorCode(errorCode);
            jsonResponse.setData(data);
            jsonResponse.setMessage(message);

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.writeValueAsString(jsonResponse);
            } catch (JsonProcessingException e) {
                return "";
            }
        }
    }

}
