package com.authenticator.modal;

/**
 * @author DEEPAK SANEM
 * @date 4/19/17.
 */
public enum RequestStatus {
    SUCCESS,
    FAIL,
    ERROR
}
