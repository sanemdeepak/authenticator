package com.authenticator.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.hibernate.validator.constraints.NotEmpty;

import java.sql.Timestamp;

/**
 * Created by sanemdeepak on 3/7/17.
 */

@JsonRootName(value = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonIgnore
    private String userLinkId;

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotEmpty
    private String email;
    @NotEmpty
    private String phoneNumber;
    @NotEmpty
    private String address;

    @JsonIgnore
    private String orgId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonIgnore
    private Boolean isActive;

    @JsonIgnore
    private Boolean isDeleted;

    @JsonIgnore
    private Timestamp lastLoginTimestamp;

    @JsonIgnore
    private Timestamp lastLogoutTimestamp;

    @JsonIgnore
    private Timestamp lastPasswordChangeTimestamp;

    @JsonIgnore
    private Timestamp lastTokenIssuedTimestamp;

    public String getUserLinkId() {
        return userLinkId;
    }

    public void setUserLinkId(String userLinkId) {
        this.userLinkId = userLinkId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Timestamp getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public Timestamp getLastLogoutTimestamp() {
        return lastLogoutTimestamp;
    }

    public void setLastLogoutTimestamp(Timestamp lastLogoutTimestamp) {
        this.lastLogoutTimestamp = lastLogoutTimestamp;
    }

    public Timestamp getLastPasswordChangeTimestamp() {
        return lastPasswordChangeTimestamp;
    }

    public void setLastPasswordChangeTimestamp(Timestamp lastPasswordChangeTimestamp) {
        this.lastPasswordChangeTimestamp = lastPasswordChangeTimestamp;
    }

    public Timestamp getLastTokenIssuedTimestamp() {
        return lastTokenIssuedTimestamp;
    }

    public void setLastTokenIssuedTimestamp(Timestamp lastTokenIssuedTimestamp) {
        this.lastTokenIssuedTimestamp = lastTokenIssuedTimestamp;
    }

    @Override
    public String toString() {
        return "User{" +
                "userLinkId='" + userLinkId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", orgId='" + orgId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isActive=" + isActive +
                ", isDeleted=" + isDeleted +
                ", lastLoginTimestamp=" + lastLoginTimestamp +
                ", lastLogoutTimestamp=" + lastLogoutTimestamp +
                ", lastPasswordChangeTimestamp=" + lastPasswordChangeTimestamp +
                ", lastTokenIssuedTimestamp=" + lastTokenIssuedTimestamp +
                '}';
    }
}
