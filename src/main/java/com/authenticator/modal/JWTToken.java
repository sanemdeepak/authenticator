package com.authenticator.modal;

import java.util.Date;

/**
 * Created by sanemdeepak on 4/1/17.
 */
public class JWTToken {

    private String username;
    private Date issuedAt;
    private Date expiresAt;
    private String issuer;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @Override
    public String toString() {
        return "JWTToken{" +
                "username='" + username + '\'' +
                ", issuedAt=" + issuedAt +
                ", expiresAt=" + expiresAt +
                ", issuer='" + issuer + '\'' +
                '}';
    }
}
