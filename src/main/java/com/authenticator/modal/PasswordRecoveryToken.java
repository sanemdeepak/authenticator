package com.authenticator.modal;

import java.sql.Timestamp;

/**
 * @author DEEPAK SANEM
 * @date 5/31/17.
 */
public class PasswordRecoveryToken {

    String recoveryTokenKey;
    String userName;
    Integer tryCount;
    Timestamp tokenExpiry;
    Boolean active;


    public String getRecoveryTokenKey() {
        return recoveryTokenKey;
    }

    public void setRecoveryTokenKey(String recoveryTokenKey) {
        this.recoveryTokenKey = recoveryTokenKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getTryCount() {
        return tryCount;
    }

    public void setTryCount(Integer tryCount) {
        this.tryCount = tryCount;
    }

    public Timestamp getTokenExpiry() {
        return tokenExpiry;
    }

    public void setTokenExpiry(Timestamp tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "PasswordRecoveryToken{" +
                "recoveryTokenKey='" + recoveryTokenKey + '\'' +
                ", userName='" + userName + '\'' +
                ", tryCount=" + tryCount +
                ", tokenExpiry=" + tokenExpiry +
                ", active=" + active +
                '}';
    }
}
