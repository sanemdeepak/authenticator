package com.authenticator.modal;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by sanemdeepak on 3/7/17.
 */

@JsonRootName(value = "permission")
public class Permission {

    private String permissionId;
    private String orgId;
    private String permission;
    private Boolean active;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "permissionId='" + permissionId + '\'' +
                ", orgId='" + orgId + '\'' +
                ", permission='" + permission + '\'' +
                ", active=" + active +
                '}';
    }
}
