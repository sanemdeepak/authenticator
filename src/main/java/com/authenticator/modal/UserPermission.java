package com.authenticator.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by sanemdeepak on 3/7/17.
 */
public class UserPermission {

    @JsonIgnore
    private String permissionId;
    private String userLinkId;
    private String permission;
    private Boolean active;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getUserLinkId() {
        return userLinkId;
    }

    public void setUserLinkId(String userLinkId) {
        this.userLinkId = userLinkId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "UserPermission{" +
                "permissionId='" + permissionId + '\'' +
                ", userLinkId='" + userLinkId + '\'' +
                ", permission='" + permission + '\'' +
                ", active=" + active +
                '}';
    }
}
