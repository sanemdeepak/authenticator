package com.authenticator.service;

import com.authenticator.exception.AuthenticatorException;
import com.authenticator.exception.AuthenticatorServiceException;
import com.authenticator.modal.JWTToken;
import com.authenticator.repository.CacheRepository;
import com.authenticator.utilities.jwt.JWTUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * Created by sanemdeepak on 4/2/17.
 */

@Service
public class TokenAuthService {

    private final Logger logger = LoggerFactory.getLogger(TokenAuthService.class);

    private CacheRepository cacheRepository;
    private JWTUtil jwtUtil;


    @Autowired
    public TokenAuthService(CacheRepository cacheRepository,
                            JWTUtil jwtUtil) {
        this.cacheRepository = cacheRepository;
        this.jwtUtil = jwtUtil;
    }

    public Optional<String> isAuthenticated(String header) throws AuthenticatorException {
        if (StringUtils.isEmpty(header)) {
            return Optional.empty();
        }
        String token = StringUtils.substringAfter(header, "bearer").trim();

        try {
            JWTToken jwtToken = jwtUtil.parseJWT(token);
            boolean result = authenticate(jwtToken.getUsername(), token);

            if (result) {
                return Optional.of(jwtToken.getUsername());
            }
        } catch (ExpiredJwtException exp) {
            logger.info("Token provided expired: " + exp);
            throw new AuthenticatorServiceException(exp);
        } catch (MalformedJwtException jwtExp) {
            logger.info("Invalid JWT header: " + header);
            throw new AuthenticatorServiceException(jwtExp);
        } catch (Exception e) {
            throw new AuthenticatorServiceException(e);
        }

        return Optional.empty();
    }


    private boolean authenticate(String username, String token) throws ExpiredJwtException, MalformedJwtException {

        Optional<Map<String, String>> cachedTokenListResult = cacheRepository.getTokens(username);

        if (!cachedTokenListResult.isPresent()) {
            logger.info("No tokens were found for username: " + username);
            return false;
        }

        String cachedTokenStr = null;
        Map<String, String> cachedTokenList = cachedTokenListResult.get();

        for (Map.Entry<String, String> entry : cachedTokenList.entrySet()) {
            if (StringUtils.equals(entry.getValue(), token)) {
                cachedTokenStr = entry.getValue();
            }
        }

        if (StringUtils.isEmpty(cachedTokenStr)) {
            logger.info("Token not found for username: " + username);
            return false;
        }

        jwtUtil.parseJWT(cachedTokenStr);

        logger.info("Authenticated user: {} with token: ", username);
        return true;
    }
}
