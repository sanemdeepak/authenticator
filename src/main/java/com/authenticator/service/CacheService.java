package com.authenticator.service;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.exception.AuthenticatorServiceException;
import com.authenticator.repository.CacheRepository;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

/**
 * @author DEEPAK SANEM
 * @date 4/19/17.
 */
@Service
public class CacheService {

    private final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private CacheRepository cacheRepository;

    @Value("${jwt.max.token.count}")
    protected Integer MAX_TOKEN_COUNT;

    @Autowired
    protected CacheService(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public boolean setToken(String jwtToken, String username) throws AuthenticatorServiceException {
        try {

            //TODO: ADD hystrix

            return cacheRepository.setToken(jwtToken, username);
        } catch (AuthenticatorResourceException are) {
            logger.warn("Exception while executing setToken(): ", ExceptionUtils.getStackTrace(are));
            return false;
        } catch (Exception exp) {
            logger.warn("Exception while executing setToken(): ", ExceptionUtils.getStackTrace(exp));
            throw new AuthenticatorServiceException(exp);
        }
    }

    public boolean unsetToken(String jwtTokenId, String username) throws AuthenticatorServiceException {
        try {
            return cacheRepository.unsetToken(jwtTokenId, username);
        } catch (AuthenticatorResourceException are) {
            logger.warn("Exception while executing unsetToken(): ", ExceptionUtils.getStackTrace(are));
            return false;
        } catch (Exception exp) {
            logger.warn("Exception while executing unsetToken(): ", ExceptionUtils.getStackTrace(exp));
            throw new AuthenticatorServiceException(exp);
        }
    }

    public Optional<Map<String, String>> getTokens(String username) throws AuthenticatorServiceException {
        try {
            return cacheRepository.getTokens(username);
        } catch (AuthenticatorResourceException are) {
            logger.warn("Exception while executing getTokens(): ", ExceptionUtils.getStackTrace(are));
            return Optional.empty();
        } catch (Exception exp) {
            logger.warn("Exception while executing getTokens(): ", ExceptionUtils.getStackTrace(exp));
            throw new AuthenticatorServiceException(exp);
        }
    }
}
