package com.authenticator.service;

import com.authenticator.exception.AuthenticatorException;
import com.authenticator.repository.UserRepository;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by sanemdeepak on 4/1/17.
 */
@Service
public class BasicCredentialAuthService {

    private final Logger logger = LoggerFactory.getLogger(BasicCredentialAuthService.class);

    private UserRepository userRepository;

    protected PasswordEncoder passwordEncoder;

    @Autowired
    public BasicCredentialAuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(4);
    }

    public Optional<String> isAuthenticated(String header) throws AuthenticatorException {
        if (!StringUtils.isEmpty(header)) {
            try {
                String credentials = StringUtils.substringAfter(header, "Basic").trim();
                byte[] byteArray = Base64.decodeBase64(credentials);
                String decodedString = new String(byteArray);
                String username = decodedString.substring(0, decodedString.indexOf(":", 0));
                String password = decodedString.substring(decodedString.indexOf(":", 0) + 1, decodedString.length());

                boolean authenticate = authenticate(username, password);
                if (authenticate) {
                    return Optional.of(username);
                } else {
                    return Optional.empty();
                }
            } catch (Exception e) {
                throw new AuthenticatorException(e);
            }
        }
        logger.info("Empty Auth header. Cannot authenticate");
        return Optional.empty();
    }

    protected boolean authenticate(String username, String password) {

        Optional<String> actualPassword = userRepository.findPasswordByUsername(username);
        if (actualPassword.isPresent() &&
                passwordEncoder.matches(password, actualPassword.get())) {
            return true;
        }
        logger.info("Failed to authenticate user: {}", username);
        return false;
    }
}
