package com.authenticator.service;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.exception.AuthenticatorServiceException;
import com.authenticator.modal.Invitation;
import com.authenticator.modal.User;
import com.authenticator.repository.InvitationRepository;
import com.authenticator.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Created by sanemdeepak on 3/29/17.
 */

@Service
public class InvitationService {

    @Value("${server.url}")
    protected String applicationUrl;

    @Value("${server.port}")
    protected String applicationPort;

    private final Logger logger = LoggerFactory.getLogger(InvitationService.class);

    private InvitationRepository invitationRepository;

    private UserRepository userRepository;

    private Validator validator;

    @Autowired
    protected InvitationService(InvitationRepository invitationRepository, UserRepository userRepository,
                                Validator validator) {
        this.invitationRepository = invitationRepository;
        this.userRepository = userRepository;
        this.validator = validator;
    }

    public Optional<String> validateUserAndGenerateInvitation(User user) {

        if (user == null) {
            return Optional.empty();
        }

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.size() > 0) {
            logger.warn("Invalid user {}", user.toString());
            throw new AuthenticatorServiceException(new ConstraintViolationException(violations));
        }

        String loggedInUsername = this.getUsernameFromContext();
        Optional<String> maybeUserLinkId;

        try {
            maybeUserLinkId = userRepository.findUserLinkIdByUsername(loggedInUsername);
        } catch (AuthenticatorResourceException are) {
            logger.error("Could not find new user_link_id: {} cause: {}", loggedInUsername, ExceptionUtils.getStackTrace(are));
            throw new AuthenticatorServiceException("Could not find creators user_link_id: " + loggedInUsername);

        }

        String newUserUserLinkId = user.getUserLinkId();

        if (StringUtils.isEmpty(newUserUserLinkId)) {
            return Optional.empty();
        }

        try {
            userRepository.findUserByUserLinkId(newUserUserLinkId);
        } catch (AuthenticatorResourceException are) {
            if (ExceptionUtils.getRootCause(are) instanceof EmptyResultDataAccessException) {
                logger.warn("Could not find new user_link_id: {} cause: {}", newUserUserLinkId, ExceptionUtils.getStackTrace(are));
                return Optional.empty();
            } else {
                throw new AuthenticatorServiceException(are);
            }
        }

        String invitationId = UUID.randomUUID().toString();
        Timestamp expiryTimeStamp = Timestamp.from(Instant.now().plus(24, ChronoUnit.HOURS));

        Invitation invitation = new Invitation();
        invitation.setInvitationId(invitationId);
        invitation.setUserLinkId(newUserUserLinkId);
        invitation.setActive(true);
        invitation.setExpiryTimestamp(expiryTimeStamp);
        invitation.setCreatedByUserLinkId(maybeUserLinkId.get());

        //TODO: Post this invitation on SQS Queue to Emailr to consume

        boolean result;
        try {
            result = invitationRepository.createNewInvitation(invitation);
        } catch (AuthenticatorResourceException are) {
            logger.warn("Could not create invitation, cause: {}", ExceptionUtils.getStackTrace(are));
            throw new AuthenticatorServiceException(are);
        }

        logger.info("Created new invitation: " + invitation.toString());

        if (result) {
            return Optional.of(buildInvitationUrl(invitationId));
        }

        return Optional.empty();
    }

    public Optional<Invitation> findInvitationByInvitationId(String invitationId) {

        try {
            return invitationRepository.findInvitationByInvitationId(invitationId);
        } catch (AuthenticatorResourceException are) {
            if (ExceptionUtils.getRootCause(are) instanceof EmptyResultDataAccessException) {
                logger.warn("No invitation found with invitation_id: " + invitationId);
                return Optional.empty();
            }
            throw new AuthenticatorServiceException(are);
        }
    }

    public boolean isValidInvitation(String invitationId) {
        Optional<Invitation> maybeInvitation;

        try {
            maybeInvitation = invitationRepository.findInvitationByInvitationId(invitationId);
        } catch (AuthenticatorResourceException are) {
            if (ExceptionUtils.getRootCause(are) instanceof EmptyResultDataAccessException) {
                logger.warn("No invitation found with invitation_id: " + invitationId);
                return false;
            }
            throw new AuthenticatorServiceException(are);
        }

        if (maybeInvitation != null && maybeInvitation.isPresent()) {
            Invitation invitation = maybeInvitation.get();

            if (invitation.isActive()) {
                return true;
            }
        }
        return false;
    }

    public boolean invalidateInvitation(String invitationId) {
        try {
            return invitationRepository.invalidateInvitationByInvitationId(invitationId);
        } catch (AuthenticatorResourceException are) {
            logger.error("Failed to execute invalidateInvitation(), cause:{} ", ExceptionUtils.getStackTrace(are));
            throw new AuthenticatorServiceException(are);
        }
    }

    public boolean claimInvitation(String invitationId) {
        Timestamp now = Timestamp.from(Instant.now());
        try {
            return invitationRepository.claimInvitation(invitationId, now);
        } catch (AuthenticatorResourceException are) {
            logger.error("Failed to execute claimInvitation(), cause:{} ", ExceptionUtils.getStackTrace(are));
            throw new AuthenticatorServiceException(are);
        }
    }

    public boolean rollbackInvitationClaim(String invitationId) {
        try {
            return invitationRepository.rollbackInvitationClaim(invitationId);
        } catch (AuthenticatorResourceException are) {
            logger.error("Failed to execute rollbackInvitationClaim(), cause:{} ", ExceptionUtils.getStackTrace(are));
            throw new AuthenticatorServiceException(are);
        }
    }

    protected String buildInvitationUrl(String invitationId) {
        if (StringUtils.isEmpty(invitationId)) {
            throw new AuthenticatorServiceException("Cannot build invitation URL with Empty InvitationId");
        }
        return StringUtils.join(applicationUrl, ":", applicationPort, "/invitation/claim/", invitationId);
    }

    protected String getUsernameFromContext() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
