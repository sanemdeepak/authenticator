package com.authenticator.service;

import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.exception.AuthenticatorServiceException;
import com.authenticator.modal.PasswordRecoveryToken;
import com.authenticator.modal.User;
import com.authenticator.modal.UserPermission;
import com.authenticator.repository.UserPasswordRecoveryRepository;
import com.authenticator.repository.UserPermissionRepository;
import com.authenticator.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Created by sanemdeepak on 3/29/17.
 */

@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(4);

    private UserRepository userRepository;

    private UserPermissionRepository userPermissionRepository;

    private UserPasswordRecoveryRepository passwordRecoveryRepository;

    private Validator validator;

    protected final String ADD_USER_PERMISSION = "user.add";

    protected final String UPDATE_USER_PERMISSION = "user.update";

    protected final String DELETE_USER_PERMISSION = "user.delete";

    @Autowired
    public UserService(UserRepository userRepository,
                       UserPermissionRepository userPermissionRepository,
                       UserPasswordRecoveryRepository passwordRecoveryRepository,
                       Validator validator) {

        this.userRepository = userRepository;
        this.userPermissionRepository = userPermissionRepository;
        this.passwordRecoveryRepository = passwordRecoveryRepository;
        this.validator = validator;
    }

    public Optional<User> getUserByUserLinkId(String userLinkId) throws AuthenticatorServiceException {

        if (StringUtils.isEmpty(userLinkId)) {
            return Optional.empty();
        }

        try {
            return userRepository.findUserByUserLinkId(userLinkId);
        } catch (AuthenticatorResourceException exp) {
            if (ExceptionUtils.getRootCause(exp) instanceof EmptyResultDataAccessException) {
                logger.warn("Could not find userLinkId {}", userLinkId);
                return Optional.empty();
            }
            throw new AuthenticatorServiceException(exp);
        }
    }

    public Optional<User> addNewUser(User user, boolean isAnonymous) throws AuthenticatorServiceException {

        boolean isAllowed;

        if (user == null) {
            return Optional.empty();
        }

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.size() > 0) {
            logger.warn("Invalid user {}", user.toString());
            throw new AuthenticatorServiceException(new ConstraintViolationException(violations));
        }

        if (!isAnonymous) {
            String loggedInUsername = getUsernameFromContext();
            Optional<String> loggedInUserUserLinkId;
            try {
                loggedInUserUserLinkId = userRepository.findUserLinkIdByUsername(loggedInUsername);
            } catch (AuthenticatorResourceException are) {
                if (ExceptionUtils.getRootCause(are) instanceof EmptyResultDataAccessException) {
                    logger.error("Could not find user_link_id of logged in user with username: {}", loggedInUsername);
                    throw are;
                }

                throw new AuthenticatorServiceException(are);
            }

            isAllowed = checkPermissionExists(loggedInUserUserLinkId.get(), ADD_USER_PERMISSION);

            if (isAllowed) {

                String tempUsername = passwordEncoder.encode(UUID.randomUUID().toString()).substring(0, 15);
                String tempPassword = passwordEncoder.encode(UUID.randomUUID().toString());

                Optional<String> maybeOrgId = userRepository.findOrgIdByUsername(loggedInUsername);

                if (!maybeOrgId.isPresent()) {
                    logger.error("No org_id for username [{}]", loggedInUsername);
                    throw new AuthenticatorServiceException("No org_id for username[" + loggedInUsername + "]");
                }

                user.setUserLinkId(UUID.randomUUID().toString());
                user.setOrgId(maybeOrgId.get());
                user.setUsername(tempUsername);
                user.setPassword(tempPassword);
                user.setActive(true);
                user.setDeleted(false);
            } else {
                logger.info("User does not have required permission: {}", ADD_USER_PERMISSION);
                return Optional.empty();
            }

        } else {
            String password = passwordEncoder.encode(user.getPassword());

            user.setUserLinkId(UUID.randomUUID().toString());
            user.setPassword(password);
            user.setOrgId("1fec5d27-ee49-4191-93e2-a868441ace58");
            user.setActive(true);
            user.setDeleted(false);
        }
        return userRepository.createUser(user);
    }

    public Optional<User> updateUser(String userLinkId, User user, boolean isAnonymous) {

        boolean isAllowed = false;

        if (user == null) {
            return Optional.empty();
        }

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (violations.size() > 0) {
            logger.warn("Invalid user {}", user.toString());
            throw new ConstraintViolationException(violations);
        }


        if (!isAnonymous) {

            String loggedInUserLinkId = getUserLinkIdFromContext();

            if (!StringUtils.equals(loggedInUserLinkId, userLinkId)) {
                isAllowed = checkPermissionExists(loggedInUserLinkId, UPDATE_USER_PERMISSION);
            } else {
                isAllowed = true;
            }
        }

        if (!isAnonymous && !isAllowed) {
            return Optional.empty();
        }
        try {
            return userRepository.updateUserDetails(user, userLinkId);
        } catch (AuthenticatorResourceException are) {
            if (ExceptionUtils.getRootCause(are) instanceof EmptyResultDataAccessException) {
                logger.error("Error updating user with userLinkId: {}", userLinkId);
                throw are;
            }

            throw new AuthenticatorServiceException(are);
        }
    }

    public boolean updateUserUsername(String userLinkId, String username) {
        if (StringUtils.isEmpty(userLinkId) || StringUtils.isEmpty(username)) {
            return false;
        }

        return userRepository.updateUserUsername(userLinkId, username);
    }

    public boolean updateUserPassword(String userLinkId, String password) {
        if (StringUtils.isEmpty(userLinkId) || StringUtils.isEmpty(password)) {
            return false;
        }

        String encryptedPassword = passwordEncoder.encode(password);

        return userRepository.updateUserPassword(userLinkId, encryptedPassword);
    }

    public boolean activateUser(String userLinkId, boolean isAnonymous) {

        boolean isAllowed = false;
        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        if (!isAnonymous) {
            String loggedInUserLinkId = getUserLinkIdFromContext();

            if (!StringUtils.equals(loggedInUserLinkId, userLinkId)) {
                isAllowed = checkPermissionExists(loggedInUserLinkId, UPDATE_USER_PERMISSION);
            } else {
                isAllowed = true;
            }
        }

        if (!isAnonymous && !isAllowed) {
            return false;
        }

        return userRepository.activateUser(userLinkId);
    }

    public boolean deactivateUser(String userLinkId, boolean isAnonymous) {
        boolean isAllowed = false;
        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        if (!isAnonymous) {

            String loggedInUserLinkId = getUserLinkIdFromContext();

            if (!StringUtils.equals(loggedInUserLinkId, userLinkId)) {
                isAllowed = checkPermissionExists(loggedInUserLinkId, UPDATE_USER_PERMISSION);
            } else {
                isAllowed = true;
            }
        }

        if (!isAnonymous && !isAllowed) {
            return false;
        }

        return userRepository.deactivateUser(userLinkId);
    }

    public boolean deleteUserByUserLinkId(String userLinkId) throws Exception {


        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        return userRepository.deleteUserByUserLinkId(userLinkId);
    }

    public boolean markUserDeleted(String userLinkId) throws Exception {

        boolean isAllowed;
        if (StringUtils.isEmpty(userLinkId)) {
            return false;
        }

        String loggedInUserLinkId = getUserLinkIdFromContext();
        if (!StringUtils.equals(loggedInUserLinkId, userLinkId)) {
            isAllowed = checkPermissionExists(loggedInUserLinkId, DELETE_USER_PERMISSION);
        } else {
            isAllowed = true;
        }

        if (isAllowed) {
            return userRepository.markUserDeleted(userLinkId);
        }

        return false;
    }

    public Optional<PasswordRecoveryToken> createPasswordRecoveryToken(String email) {

        Optional<String> usernameByEmail = userRepository.findUsernameByEmail(email);

        if (!usernameByEmail.isPresent()) {
            logger.warn("No user with email {}", email);
            return Optional.empty();
        }

        String randomId = UUID.randomUUID().toString();
        Timestamp expiryTimestamp = Timestamp.from(Instant.now().plus(24, ChronoUnit.HOURS));

        PasswordRecoveryToken recoveryToken = new PasswordRecoveryToken();
        recoveryToken.setUserName(usernameByEmail.get());
        recoveryToken.setActive(true);
        recoveryToken.setTokenExpiry(expiryTimestamp);
        recoveryToken.setTryCount(0);
        recoveryToken.setRecoveryTokenKey(randomId);
        try {
            return passwordRecoveryRepository.addNewRecoveryToken(recoveryToken);
        } catch (AuthenticatorResourceException authenticatorResourceException) {
            logger.error("Error executing createPasswordRecoveryToken due to: {}", ExceptionUtils.getStackTrace(authenticatorResourceException));
            throw new AuthenticatorServiceException(authenticatorResourceException);
        }
    }

    public Optional<Boolean> recoverPassword(String recoveryTokenKey, String newPassword) {

        Optional<PasswordRecoveryToken> recoveryTokenByTokenKey = passwordRecoveryRepository.findRecoveryTokenByTokenKey(recoveryTokenKey);

        if (!recoveryTokenByTokenKey.isPresent() ||
                !recoveryTokenByTokenKey.get().getActive() ||
                recoveryTokenByTokenKey.get().getTokenExpiry().before(Timestamp.from(Instant.now()))) {
            return Optional.empty();
        }

        String userLinkId = userRepository.findUserLinkIdByUsername(recoveryTokenByTokenKey.get().getUserName()).get();

        passwordRecoveryRepository.deactivateRecoveryToken(recoveryTokenKey);

        return Optional.of(updateUserPassword(userLinkId, newPassword));

    }

    protected boolean checkPermissionExists(String userLinkId, String permission) {
        Optional<List<UserPermission>> allUserPermissionsByUserLinkId = userPermissionRepository.findAllUserPermissionsByUserLinkId(userLinkId);

        for (UserPermission userPermission : allUserPermissionsByUserLinkId.get()) {
            if (StringUtils.equalsIgnoreCase(userPermission.getPermission(), permission)) {
                return true;
            }
        }

        return false;
    }

    protected String getUsernameFromContext() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    protected String getUserLinkIdFromContext() {
        String loggedInUsername = getUsernameFromContext();

        Optional<String> userLinkIdByUsername = userRepository.findUserLinkIdByUsername(loggedInUsername);
        return userLinkIdByUsername.orElse(null);
    }

}
