package com.authenticator.resource;

import com.authenticator.modal.JSONResponse;
import com.authenticator.modal.RequestStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author DEEPAK SANEM
 * @date 4/19/17.
 */
@RestController
public class PingResource {

    @RequestMapping(
            value = "/ping",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity<String> ping() throws Exception {

        Timestamp now = Timestamp.from(Instant.now());
        RequestStatus status = RequestStatus.SUCCESS;

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withData("pong")
                .buildResponseAsString();

        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/auth/test",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity<String> authTest() throws JsonProcessingException {

        Timestamp now = Timestamp.from(Instant.now());
        RequestStatus status = RequestStatus.SUCCESS;

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withData("Authenticated")
                .buildResponseAsString();

        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
}
