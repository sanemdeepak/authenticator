package com.authenticator.resource;

import com.authenticator.service.CacheService;
import com.authenticator.utilities.jwt.JWTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.authenticator.utilities.ResponseBuilders.createBadRequestResponse;
import static com.authenticator.utilities.ResponseBuilders.createInternalServerErrorResponse;
import static com.authenticator.utilities.ResponseBuilders.createSuccessResponse;

/**
 * Created by sanemdeepak on 4/3/17.
 */

@RestController
@RequestMapping("/token")
public class TokenResource {

    private final Logger logger = LoggerFactory.getLogger(TokenResource.class);

    @Autowired
    protected ObjectMapper objectMapper;

    protected JWTUtil jwtUtil;
    protected CacheService cacheService;

    @Autowired
    public TokenResource(CacheService cacheService, JWTUtil jwtUtil) {
        this.cacheService = cacheService;
        this.jwtUtil = jwtUtil;
    }


    @RequestMapping(
            value = "/new",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    @ResponseBody
    @CrossOrigin
    public ResponseEntity<String> requestAndSetNewToken() throws Exception {

        String username = getUsernameFromContext();

        String token = jwtUtil.generateTokenFrom(username);
        try {
            cacheService.setToken(token, username);
        } catch (Exception exp) {
            logger.error("Error updating cache, username: {} due to {}", username, exp);
            throw new Exception(exp);
        }

        token = StringUtils.join("bearer", " ", token);

        return createSuccessResponse(token);
    }

    @RequestMapping(
            value = "/remove",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity<String> removeToken(@PathParam("tokenId") String tokenId) throws Exception {

        String username = getUsernameFromContext();

        try {
            boolean result = cacheService.unsetToken(tokenId, username);
            if (result) {
                return createSuccessResponse("Successfully removed token with tokenId: " + tokenId);
            }
        } catch (Exception exp) {
            logger.error("Error updating cache, username: {} due to {}", username, exp);
            throw exp;
        }

        return createBadRequestResponse("Error removing token with tokenId: " + tokenId);
    }


    @RequestMapping(
            value = "/tokens",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity getAllTokens() {

        String username = getUsernameFromContext();

        try {
            Optional<Map<String, String>> maybeTokens = cacheService.getTokens(username);

            Map<String, String> tokens = maybeTokens.orElse(Collections.emptyMap());

            String responseStr = objectMapper.writeValueAsString(tokens);

            return createSuccessResponse(responseStr);

        } catch (IOException e) {
            return createInternalServerErrorResponse(e);
        }
    }

    @RequestMapping(
            value = "/count",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity getTokenCount() {

        String username = getUsernameFromContext();

        Optional<Map<String, String>> maybeTokens = cacheService.getTokens(username);

        Map<String, String> tokens = maybeTokens.orElse(Collections.emptyMap());

        return createSuccessResponse("" + tokens.size());

    }

    private String getUsernameFromContext() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
