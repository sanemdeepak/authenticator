package com.authenticator.resource;

import com.authenticator.modal.PasswordRecoveryToken;
import com.authenticator.modal.RequestStatus;
import com.authenticator.modal.User;
import com.authenticator.service.InvitationService;
import com.authenticator.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

import static com.authenticator.utilities.ResponseBuilders.*;

/**
 * Created by sanemdeepak on 4/19/17.
 */

@RestController
@RequestMapping("/user")
public class UserResource {

    private final Logger logger = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;

    private final InvitationService invitationService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    public UserResource(UserService userService, InvitationService invitationService) {
        this.userService = userService;
        this.invitationService = invitationService;
    }

    @RequestMapping(
            value = "/{userLinkId}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity findUserByUserLinkId(@PathVariable("userLinkId") String userLinkId) throws Exception {
        String jsonResponse;
        RequestStatus status;
        if (StringUtils.isEmpty(userLinkId)) {
            return createBadRequestResponse("userLinkId cannot be empty");
        }

        Optional<User> maybeUser = userService.getUserByUserLinkId(userLinkId);

        if (maybeUser.isPresent()) {

            String userAsString = objectMapper.writeValueAsString(maybeUser.get());

            return createSuccessResponse(userAsString);
        } else {
            return createResponse(RequestStatus.FAIL, "No user found with userLinkId: " + userLinkId, null, HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = "/new",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity addNewUser(@RequestBody @Valid User user) throws Exception {

        Optional<User> maybeUser;

        String userAsString;

        userAsString = objectMapper.writeValueAsString(user);

        // Create new user
        try {
            maybeUser = userService.addNewUser(user, false);
        } catch (Exception exp) {
            if (exp instanceof ConstraintViolationException) {
                return createBadRequestResponse(((ConstraintViolationException) exp).getConstraintViolations().toString());
            }

            logger.error("Error creating user", exp);
            return createInternalServerErrorResponse(exp);
        }

        if (maybeUser.isPresent()) {
            // Create new invitation
            try {
                Optional<String> maybeInvitation = invitationService.validateUserAndGenerateInvitation(maybeUser.get());

                if (maybeInvitation.isPresent()) {

                    String newUserAsString = bindInvitationWithUser(userAsString, maybeInvitation.get());

                    return createSuccessResponse(newUserAsString);
                } else {
                    logger.error("Error invitation for userLinkId: {}", maybeUser.get().getUserLinkId());
                    //rollback
                    userService.deleteUserByUserLinkId(maybeUser.get().getUserLinkId());
                }
            } catch (Exception exp) {
                logger.error("Error invitation for userLinkId: " + maybeUser.get().getUserLinkId(), exp);
                //rollback
                userService.deleteUserByUserLinkId(maybeUser.get().getUserLinkId());
                throw exp;
            }
        }
        return createResponse(RequestStatus.FAIL, userAsString, null, null);
    }


    @RequestMapping(
            value = "/signup",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity signupUser(@RequestBody @Valid User user) throws Exception {

        Optional<User> maybeUser;

        String userAsString;

        userAsString = objectMapper.writeValueAsString(user);

        // Create new user
        try {
            maybeUser = userService.addNewUser(user, true);
        } catch (Exception exp) {
            if (exp instanceof ConstraintViolationException) {
                return createBadRequestResponse(((ConstraintViolationException) exp).getConstraintViolations().toString());
            }

            logger.error("Error creating user", exp);
            return createInternalServerErrorResponse(exp);
        }

        if (maybeUser.isPresent()) {

            return createSuccessResponse("Successfully created with user: " + userAsString);
        } else {
            logger.error("Error invitation for userLinkId: {}", maybeUser.get().getUserLinkId());
            //rollback
            userService.deleteUserByUserLinkId(maybeUser.get().getUserLinkId());
        }

        return createResponse(RequestStatus.FAIL, userAsString, null, null);

    }

    @RequestMapping(
            value = "/update/{userLinkId}",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity updateUser(@PathVariable("userLinkId") String userLinkId,
                                     @RequestBody @Valid User user) throws Exception {

        Optional<User> maybeUser;

        String userAsString;

        userAsString = objectMapper.writeValueAsString(user);

        // update user
        try {
            maybeUser = userService.updateUser(userLinkId, user, false);
        } catch (Exception exp) {
            if (exp instanceof ConstraintViolationException) {
                createBadRequestResponse(((ConstraintViolationException) exp).getConstraintViolations().toString());
            }
            return createInternalServerErrorResponse(exp);
        }

        if (maybeUser.isPresent()) {
            return createSuccessResponse(userAsString);
        }

        return createResponse(RequestStatus.FAIL, "Failed to update user" + userAsString, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(
            value = "/delete/{userLinkId}",
            method = RequestMethod.DELETE,
            consumes = "application/json",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity deleteUser(@PathVariable("userLinkId") String userLinkId) throws Exception {

        if (StringUtils.isEmpty(userLinkId)) {
            return createBadRequestResponse("empty userLinkId");
        }

        boolean result;

        // delete user
        try {
            result = userService.markUserDeleted(userLinkId);
        } catch (Exception exp) {
            return createInternalServerErrorResponse(exp);
        }

        if (result) {
            return createSuccessResponse("Deleted user: " + userLinkId);
        }

        return createResponse(RequestStatus.FAIL, "Failed to delete user" + userLinkId, null, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/recover/generate/token",
            method = RequestMethod.PUT,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity createRecoveryToken(@RequestParam("email") String email) {
        if (StringUtils.isEmpty(email)) {
            return createBadRequestResponse("empty email");
        }

        try {
            Optional<PasswordRecoveryToken> maybePasswordRecoveryToken = userService.createPasswordRecoveryToken(email);

            if (maybePasswordRecoveryToken.isPresent()) {
                return createSuccessResponse("Recovery token is: " + maybePasswordRecoveryToken.get().getRecoveryTokenKey());
            }
        } catch (Exception exp) {
            return createInternalServerErrorResponse(exp);
        }

        logger.error("Error creating recovery token for email: {}", email);
        return createResponse(RequestStatus.FAIL, null, "Failed to create recovery token", HttpStatus.OK);
    }


    @RequestMapping(
            value = "/recover/password",
            method = RequestMethod.PUT,
//            consumes = "application/json",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity recoverPassword(@RequestParam("token") String token,
                                          @RequestBody String body) {

        if (StringUtils.isEmpty(token)) {
            return createBadRequestResponse("empty token");
        }

        Optional<Boolean> maybeSuccess = null;

        try {
            JSONObject jsonObject = new JSONObject(body);
            String newPassword = jsonObject.getString("password");
            maybeSuccess = userService.recoverPassword(token, newPassword);
        } catch (Exception exp) {
            return createInternalServerErrorResponse(exp);
        }

        if (!maybeSuccess.isPresent() ||
                (maybeSuccess.get() == Boolean.FALSE)) {
            return createResponse(RequestStatus.FAIL, null, "Recovery token invalid", HttpStatus.OK);
        }

        return createSuccessResponse("Your password is updated, please login again");
    }

    private String bindInvitationWithUser(String userAsString, String invitation) throws IOException {

        ObjectNode objectNode = (ObjectNode) objectMapper.readTree(userAsString);
        objectNode.put("invitationUrl", invitation);
        return objectMapper.writeValueAsString(objectNode);
    }
}
