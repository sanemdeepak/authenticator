package com.authenticator.resource;

import com.authenticator.exception.AuthenticatorException;
import com.authenticator.modal.Invitation;
import com.authenticator.modal.RequestStatus;
import com.authenticator.modal.User;
import com.authenticator.service.InvitationService;
import com.authenticator.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

import static com.authenticator.utilities.ResponseBuilders.createBadRequestResponse;
import static com.authenticator.utilities.ResponseBuilders.createResponse;
import static com.authenticator.utilities.ResponseBuilders.createSuccessResponse;

/**
 * Created by sanemdeepak on 4/3/17.
 */

@SuppressWarnings("Duplicates")
@Api(
        basePath = "/invitation",
        value = "Invitation",
        description = "Operations related to invitations",
        produces = "application/json"
)

@RestController
@RequestMapping("/invitation")
public class InvitationResource {

    protected InvitationService invitationService;
    protected UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    public InvitationResource(InvitationService invitationService, UserService userService) {
        this.invitationService = invitationService;
        this.userService = userService;
    }


    @ApiOperation(value = "Check if given invitationId is valid")
    @RequestMapping(
            value = "/check/{invitationid}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "valid", response = String.class),
            @ApiResponse(code = 400, message = "empty invitationId", response = String.class),
    })
    public ResponseEntity<String> isValidInvitation(@PathVariable("invitationid") String maybeInvitationId) throws Exception {

        if (StringUtils.isEmpty(maybeInvitationId)) {
            return createBadRequestResponse("Empty invitationId");
        }

        boolean result = invitationService.isValidInvitation(maybeInvitationId);
        if (result) {
            Optional<Invitation> invitationById = invitationService.findInvitationByInvitationId(maybeInvitationId);

            Optional<User> maybeUser = userService.getUserByUserLinkId(invitationById.orElseThrow(() ->
                    new AuthenticatorException("Cannot find invitation with id" + maybeInvitationId)).getUserLinkId());

            if (invitationById.get().isActive()) {
                return createSuccessResponse("Invitation ID is valid");
            }
        }

        return createResponse(RequestStatus.FAIL, "Invitation ID is invalid", null, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/invalidate/{invitationid}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity<String> invalidateInvitation(@PathVariable("invitationid") String maybeInvitationId) throws Exception {

        if (StringUtils.isEmpty(maybeInvitationId)) {
            return createBadRequestResponse("Empty invitationId");
        }

        boolean result = invitationService.invalidateInvitation(maybeInvitationId);

        if (result) {

            return createSuccessResponse("Invalidated invitation with invitation id: " + maybeInvitationId);
        }

        return createResponse(RequestStatus.FAIL, "Try again", null, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/claim/{invitationid}",
            method = RequestMethod.POST,
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity<?> claimInvitationToken(@PathVariable("invitationid") String maybeInvitationId,
                                                  @RequestBody @Valid User maybeUser) throws Exception {

        boolean isClaimSuccess = false;

        if (StringUtils.isEmpty(maybeInvitationId) ||
                StringUtils.isEmpty(maybeUser.getUsername()) ||
                StringUtils.isEmpty(maybeUser.getPassword())) {

            return createBadRequestResponse("Missing required data");
        }

        boolean isValidInvitation = invitationService.isValidInvitation(maybeInvitationId);

        if (!isValidInvitation) {
            return createResponse(RequestStatus.FAIL, null, "Invitation has already been claimed ", HttpStatus.OK);
        }

        try {
            boolean claimResult = invitationService.claimInvitation(maybeInvitationId);

            Optional<Invitation> invitationById = invitationService.findInvitationByInvitationId(maybeInvitationId);

            if (claimResult) {

                String userLinkId = invitationById.get().getUserLinkId();

                userService.activateUser(userLinkId, true);

                Optional<User> updatedUser = userService.updateUser(userLinkId, maybeUser, true);

                if (userService.updateUserUsername(userLinkId, maybeUser.getUsername()) &&
                        userService.updateUserPassword(userLinkId, maybeUser.getPassword())) {

                    String updatedUserAsString = objectMapper.writeValueAsString(updatedUser.get());
                    isClaimSuccess = true;
                    return createSuccessResponse(updatedUserAsString);

                }
            }
        } finally {
            if (!isClaimSuccess) {
                invitationService.rollbackInvitationClaim(maybeInvitationId);
            }
        }

        return createResponse(RequestStatus.FAIL, "Try again", null, HttpStatus.OK);
    }
}
