package com.authenticator.utilities.rowMappers;

import com.authenticator.modal.PasswordRecoveryToken;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author DEEPAK SANEM
 * @date 5/31/17.
 */
@Component
public class PasswordRecoveryTokenRowMapper implements RowMapper<PasswordRecoveryToken> {
    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public PasswordRecoveryToken mapRow(ResultSet rs, int rowNum) throws SQLException {

        PasswordRecoveryToken passwordRecoveryToke = new PasswordRecoveryToken();

        passwordRecoveryToke.setRecoveryTokenKey(rs.getString("recovery_token_key"));
        passwordRecoveryToke.setUserName(rs.getString("user_name"));
        passwordRecoveryToke.setTryCount(rs.getInt("try_count"));
        passwordRecoveryToke.setTokenExpiry(rs.getTimestamp("token_expiry"));
        passwordRecoveryToke.setActive(rs.getBoolean("active"));

        return passwordRecoveryToke;
    }
}
