package com.authenticator.utilities.rowMappers;

import com.authenticator.modal.Organization;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sanemdeepak on 3/12/17.
 */
@Component
public class OrganizationRowMapper implements RowMapper<Organization> {
    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Organization mapRow(ResultSet rs, int rowNum) throws SQLException {
        Organization organization = new Organization();
        organization.setOrgId(rs.getString("org_id"));
        organization.setOrgName(rs.getString("org_name"));
        organization.setAddress(rs.getString("address"));
        organization.setPhoneNumber(rs.getString("phone_no"));
        organization.setEmail(rs.getString("email"));
        organization.setCreatedOn(rs.getTimestamp("created_on"));
        organization.setActive(rs.getBoolean("active"));

        return organization;
    }
}
