package com.authenticator.utilities.rowMappers;

import com.authenticator.modal.Invitation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sanemdeepak on 3/12/17.
 */
@Component
public class InvitationRowMapper implements RowMapper<Invitation> {
    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Invitation mapRow(ResultSet rs, int rowNum) throws SQLException {
        Invitation invitation = new Invitation();
        invitation.setInvitationId(rs.getString("invitation_id"));
        invitation.setUserLinkId(rs.getString("user_link_id"));
        invitation.setActive(rs.getBoolean("active"));
        invitation.setExpiryTimestamp(rs.getTimestamp("expiry_timestamp"));
        invitation.setClaimedTimestamp(rs.getTimestamp("claimed_timestamp"));
        invitation.setCreatedByUserLinkId(rs.getString("created_by_user_link_id"));
        return invitation;
    }
}
