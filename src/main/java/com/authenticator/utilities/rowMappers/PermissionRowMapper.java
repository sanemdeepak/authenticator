package com.authenticator.utilities.rowMappers;

import com.authenticator.modal.Permission;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sanemdeepak on 3/12/17.
 */
@Component
public class PermissionRowMapper implements RowMapper<Permission> {
    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public Permission mapRow(ResultSet rs, int rowNum) throws SQLException {

        Permission permission = new Permission();
        permission.setPermissionId(rs.getString("permission_id"));
        permission.setOrgId(rs.getString("org_id"));
        permission.setPermission(rs.getString("permission"));
        permission.setActive(rs.getBoolean("active"));
        return null;
    }
}
