package com.authenticator.utilities.rowMappers;

import com.authenticator.modal.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sanemdeepak on 3/12/17.
 */
@Component
public class UserRowMapper implements RowMapper<User> {
    /**
     * Implementations must implement this method to map each row of data
     * in the ResultSet. This method should not call {@code next()} on
     * the ResultSet; it is only supposed to map values of the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row
     * @throws SQLException if a SQLException is encountered getting
     *                      column values (that is, there's no need to catch SQLException)
     */
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();

        user.setUserLinkId(rs.getString("user_link_id"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setEmail(rs.getString("email"));
        user.setPhoneNumber(rs.getString("phone_no"));
        user.setAddress(rs.getString("address"));
        user.setOrgId(rs.getString("org_id"));
        user.setActive(rs.getBoolean("active"));
        user.setDeleted(rs.getBoolean("deleted"));
        user.setLastLoginTimestamp(rs.getTimestamp("last_login_timestamp"));
        user.setLastLogoutTimestamp(rs.getTimestamp("last_logout_timestamp"));

        return user;
    }
}
