package com.authenticator.utilities.jwt;

import com.authenticator.modal.JWTToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * Created by sanemdeepak on 4/1/17.
 */

@Component
public class JWTUtil {

    @Value("${applicationName}")
    protected String appName;

    @Value("${jwt.secret}")
    protected String jwtSecret;

    @Value("${jwt.token.expiry.threshold}")
    protected Integer TOKEN_EXPIRY_THRESHOLD;

    protected SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

    private ObjectMapper objectMapper;

    @Autowired
    public JWTUtil(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    public String generateTokenFrom(String username) {

        long nowMillis = Instant.now().toEpochMilli();
        Date now = new Date(nowMillis);
        Date expiryTimestamp = Date.from(Instant.now().plus(TOKEN_EXPIRY_THRESHOLD, ChronoUnit.DAYS));
        byte[] apiKeySecretBytes = jwtSecret.getBytes();
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        String base64EncodedUserLinkId = Base64.encodeBase64String(username.getBytes());
        JwtBuilder builder = Jwts.builder().setId(base64EncodedUserLinkId)
                .setIssuedAt(now)
                .setExpiration(expiryTimestamp)
                .setSubject("Authorization")
                .setIssuer(appName)
                .signWith(signatureAlgorithm, signingKey);

        return builder.compact();
    }

    public JWTToken parseJWT(String jwtText) {
        byte[] apiKeySecretBytes = jwtSecret.getBytes();
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        Claims claims = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(jwtText).getBody();

        String plainUsername = new String(Base64.decodeBase64(claims.getId()));

        JWTToken jwtToken = new JWTToken();
        jwtToken.setUsername(plainUsername);
        jwtToken.setIssuedAt(claims.getIssuedAt());
        jwtToken.setIssuer(claims.getIssuer());
        jwtToken.setExpiresAt(claims.getExpiration());
        return jwtToken;
    }
}
