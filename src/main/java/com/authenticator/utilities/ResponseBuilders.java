package com.authenticator.utilities;

import com.authenticator.modal.JSONResponse;
import com.authenticator.modal.RequestStatus;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author DEEPAK SANEM
 * @date 4/24/17.
 */
public class ResponseBuilders {

    public static ResponseEntity createResponse(RequestStatus status, String data, String message, HttpStatus httpStatus) {
        Timestamp now = Timestamp.from(Instant.now());

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withData(data)
                .withMessage(message)
                .withErrorCode(httpStatus.value())
                .buildResponseAsString();
        if (httpStatus != null) {
            return new ResponseEntity<>(jsonResponse, httpStatus);
        }
        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }


    public static ResponseEntity createSuccessResponse(String data) {
        Timestamp now = Timestamp.from(Instant.now());
        RequestStatus status = RequestStatus.SUCCESS;

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withData(data)
                .buildResponseAsString();

        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }

    public static ResponseEntity createBadRequestResponse(String data) {
        Timestamp now = Timestamp.from(Instant.now());
        RequestStatus status = RequestStatus.FAIL;

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withData(data)
                .withErrorCode(HttpStatus.BAD_REQUEST.value())
                .buildResponseAsString();

        return new ResponseEntity<>(jsonResponse, HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity createInternalServerErrorResponse(Throwable exp) {
        Timestamp now = Timestamp.from(Instant.now());
        RequestStatus status = RequestStatus.ERROR;

        String jsonResponse = new JSONResponse.JSONResponseBuilder(now, status)
                .withMessage(ExceptionUtils.getStackTrace(exp))
                .withErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .buildResponseAsString();


        return new ResponseEntity<>(jsonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
