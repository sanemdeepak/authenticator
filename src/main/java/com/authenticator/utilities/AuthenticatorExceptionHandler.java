package com.authenticator.utilities;


import com.authenticator.exception.AuthenticatorException;
import com.authenticator.exception.AuthenticatorResourceException;
import com.authenticator.exception.AuthenticatorServiceException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static com.authenticator.utilities.ResponseBuilders.createBadRequestResponse;
import static com.authenticator.utilities.ResponseBuilders.createInternalServerErrorResponse;

/**
 * @author DEEPAK SANEM
 * @date 4/24/17.
 */

@ControllerAdvice
public class AuthenticatorExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity boom(Exception e) {
        e.printStackTrace();
        return createInternalServerErrorResponse(e);
    }

    @ExceptionHandler(AuthenticatorResourceException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity authenticatorResourceException(AuthenticatorResourceException e) {
        e.printStackTrace();
        return createInternalServerErrorResponse(e);
    }

    @ExceptionHandler(AuthenticatorServiceException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity authenticatorServiceException(AuthenticatorServiceException e) {
        e.printStackTrace();
        return createBadRequestResponse(ExceptionUtils.getStackTrace(e));
    }

    @ExceptionHandler(AuthenticatorException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity authenticatorException(AuthenticatorException e) {
        e.printStackTrace();
        return createInternalServerErrorResponse(e);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity badHttpMethodException(Exception e) {
        return createBadRequestResponse(ExceptionUtils.getMessage(e));
    }
}
