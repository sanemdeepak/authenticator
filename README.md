**Authenticator**
-
- An application to manage users and provide authentication tokens.

**_Setup_**

Before running the application lets setup required tables in database(MySQL).

- Execute all the SQL queries located in `/src/main/java/resources/dbSetup.sql`

- In application.properties file override spring.datasource.url `<database_url>` with url for your database and `database_name` with your database name.
example:
`jdbc:mysql://localhost>:3306/authenticator?autoReconnect=true`

provide username and password to connect to your database.

- Authenticator uses caching via redis for speedy authentication, it is not a mandatory to have caching, application would not crash if redis server is not running.

refer: https://redis.io/topics/quickstart for starting up redis server, once server is up and running provide url and port number at:
`spring.redis.host=<url>`

`spring.redis.port=<port>`

- User http://passwordsgenerator.net/ to generate a random string and set property `jwt.secret` with random generated string

**Running application**
- 

Authenticator is a gradle application so its very easy to run, just navigate to root directory and run ./gradlew run.



